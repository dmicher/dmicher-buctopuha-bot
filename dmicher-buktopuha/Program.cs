﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Threading;
using Discord;
using Discord.WebSocket;
using System.Threading.Tasks;
using dmicher_buktopuha.Infrastructure;
using dmicher_buktopuha.Infrastructure.DataBase;
using dmicher_buktopuha.Infrastructure.Quiz;
using dmicher_buktopuha.Infrastructure.Settings;

namespace dmicher_buktopuha
{
    /// <summary>
    /// Запуск программы
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Точка входа: инициализирует основной поток
        /// </summary>
        public static void Main()
            => new Program().MainAsync().GetAwaiter().GetResult();

        /// <summary>
        /// Клиент бота в дискорд
        /// </summary>
        private DiscordSocketClient _client;

        /// <summary>
        /// Ссылка на клиент Discord
        /// </summary>
        private static GameManager _gameManager;

        /// <summary>
        /// Запускает бота, добавляет ему слушателя на команды
        /// </summary>
        private async Task MainAsync()
        {
            _client = new DiscordSocketClient();
            _client.MessageReceived += CommandsHandler;
            _client.Log += Log;
            _client.Ready += () =>
            {
                // после полного запуска клиента передаёт его для запуска менеджера вопросов
                _gameManager = new GameManager(_client);
                Console.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " Dmicher     QuizManager Loaded\r\n" +
                                  "Hello there! It's Dmicher Abathur Kubrow's BUKTOPUHA bot for Discord. Contacts: dmicher.ru / dscherkasov@yandex.ru");
                return Task.CompletedTask;
            };
            SettingsManager.ReadSettings();
            await _client.LoginAsync(TokenType.Bot, SettingsManager.CurrentSettings[SettingsNames.DiscordToken]);
            await _client.StartAsync();

            string input;
            do
            {
                input = Console.ReadLine();
                Thread.Sleep(5_000);
            } while (string.IsNullOrWhiteSpace(input));
        }

        /// <summary>
        /// Логи выводит в консоль
        /// </summary>
        /// <param name="message">Входящее сообщение лога</param>
        private static Task Log(LogMessage message)
        {
            Console.WriteLine(message);
            return Task.CompletedTask;
        }

        /// <summary>
        /// Обрабатывает входящие сообщения
        /// </summary>
        /// <param name="message">Входящее сообщение</param>
        private Task CommandsHandler(SocketMessage message)
        {
            // игнорирует: запросы от ботов, запросы не из каналов 1. "для вопросов", 2. "для управления"
            if (message.Author.IsBot
                || (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordQuizzesChannelId])
                    && message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId])))
                return Task.CompletedTask;

            var content = message.Content.ToLower().Trim();

            // Получен команды (заканчиваются на восклицательный знак)
            if (content.EndsWith('!'))
            {
                content = content.Remove(content.IndexOf('!'));
                switch (content)
                {
                    #region Команды "Старт" и "Стоп" - в зависимости от настройки

                    // запуск новой серии вопросов
                    case "старт":
                        {
                            var settings = SettingsManager.CurrentSettings;

                            // Если режим авторизации для запуска не свободный для всех пользователей (а только админы), 
                            // то предварительно запускает проверки того, кто и откуда выдал команду на старт новой серии вопросов
                            if (!settings.ContainsKey(SettingsNames.GameStartAuthorization)
                                || settings[SettingsNames.GameStartAuthorization].Trim() is "1" or null or "")
                            {
                                if (settings[SettingsNames.DiscordAdministratorsIds]
                                    .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                    .All(x => ulong.Parse(x) != message.Author.Id))
                                {
                                    message.Channel.SendMessageAsync("Невозможно выполнить команду: недостаточно прав.");
                                    return Task.CompletedTask;
                                }

                                if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                                {
                                    message.Channel.SendMessageAsync("Невозможно выполнить команду: этот канал не предназначен для управления ботом.");
                                    return Task.CompletedTask;
                                }
                            }

                            // после авторизации запускает выполнение команды
                            if (_gameManager.IsActive)
                            {
                                if (!_gameManager.StopFlag)
                                    return Task.CompletedTask;
                                lock (_gameManager.StopFlagLocker)
                                {
                                    _gameManager.StopFlag = false;
                                }

                                message.Channel.SendMessageAsync(
                                    "Команда остановки снята. Серия будет продолжена после этого вопроса.");
                                return Task.CompletedTask;
                            }

                            try
                            {
                                _gameManager.Start();
                            }
                            catch (BuktopuhaException e)
                            {
                                message.Channel.SendMessageAsync(e.Message);
                            }

                            return Task.CompletedTask;
                        }

                    // остановка серии вопросов
                    case "стоп":
                        {
                            var settings = SettingsManager.CurrentSettings;

                            // Если режим авторизации для остановки не свободный для всех пользователей (а только админы), 
                            // то предварительно запускает проверки того, кто и откуда выдал команду на остановку серии вопросов
                            if (!settings.ContainsKey(SettingsNames.GameStartAuthorization)
                                || settings[SettingsNames.GameStartAuthorization].Trim() is "1" or null or "")
                            {
                                if (settings[SettingsNames.DiscordAdministratorsIds]
                                    .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                    .All(x => ulong.Parse(x) != message.Author.Id))
                                {
                                    message.Channel.SendMessageAsync("Невозможно выполнить команду: недостаточно прав.");
                                    return Task.CompletedTask;
                                }

                                if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                                {
                                    message.Channel.SendMessageAsync("Невозможно выполнить команду: этот канал не предназначен для управления ботом.");
                                    return Task.CompletedTask;
                                }
                            }

                            // Если нужно (серия запущена и флаг остановки ещё не поднят) поднимает флаг остановки
                            if (!_gameManager.IsActive)
                                return Task.CompletedTask;
                            if (_gameManager.StopFlag)
                                return Task.CompletedTask;
                            lock (_gameManager.StopFlagLocker)
                            {
                                _gameManager.StopFlag = true;
                            }

                            message.Channel.SendMessageAsync("Команда остановки принята. Это последний вопрос серии.");
                            return Task.CompletedTask;
                        }

                    #endregion

                    #region Команды для игроков в канале для вопросов

                    // вывести таблицу лидеров
                    case "топ":
                        {
                            var (success, result) = GameManager.Top();
                            message.Channel.SendMessageAsync(success ? "**Топ игроков:**" + result : "Ошибка. " + result);
                            return Task.CompletedTask;
                        }
                    // вывести статистику текущего игрока
                    case "я":
                        {
                            var expNames = SettingsManager.CurrentSettings[SettingsNames.PlayersExpName].Split(',');
                            var exp = new NumericName(expNames[0], expNames[1], expNames[2]);
                            var ans = new NumericName("ответ", "ответа", "ответов");
                            var (player, position) = DataBaseManager.ScoreByPlayer(message.Author.Id);
                            message.Channel.SendMessageAsync(position < 0
                                ? "В базе данных ещё нет игроков. Начните играть, и они появятся."
                                : $"Игрок {message.Author.Mention}. Отображаемое имя - **{player.Name}**.\n" +
                                  $"*Результаты: **{player.Score} {exp.GetStringByNumber(player.Score)}** (за {player.Answers} {ans.GetStringByNumber(player.Answers)}), " +
                                  $" **№ {position}** в таблице лидеров, максимальная серия - {player.MaxWinningStreak} {ans.GetStringByNumber(player.MaxWinningStreak)}.*");
                            return Task.CompletedTask;
                        }
                    // вывести приветственное сообщение
                    case "как":
                    case "помощь":
                        {
                            message.Channel.SendMessageAsync($"**О! Здорово, {message.Author.Mention}! " +
                                                             $"Я бот {Assembly.GetExecutingAssembly().GetName().Name} " +
                                                             $"версии {Assembly.GetExecutingAssembly().GetName().Version?.Major}.{Assembly.GetExecutingAssembly().GetName().Version?.Minor}.**\n" +
                                                             "\n**Мои команды:**" +
                                                             "\n   - **любое сообщение** *(кроме команд ниже)* - попытаться ответить на текущий вопрос викторины, если серия вопросов запущена;" +
                                                             "\n   - **я!** - посмотреть свою статистику в игре;" +
                                                             "\n   - **имя НовоеКрутоеИмя!** - использовать в качестве отображаемого имени \"НовоеКрутоеИмя\";" +
                                                             "\n   - **топ!** - посмотреть таблицу лидеров в игре;" +
                                                             "\n   - **помощь!** и **как!** - выведет это приветствие." +
                                                             "\n   - **вопрос!** + вопрос с ответом (можно с картинкой) - предложить на модерацию свой вопрос викторины." +
                                                             "\n" +
                                                             "\n**Правила игры:**" +
                                                             "\nПосле старта каждой новой серии я задаю вопросы. Все игроки на них пытаются ответить. " +
                                                             "Кто быстрее правильно ответит на вопрос, тот победитель. " +
                                                             "У некоторых вопросов есть альтернативные варианты ответа - они тоже принимаются. " +
                                                             "Отвечать на один вопрос можно неограниченное количество раз (не бойтесь \"ачипяток\"). " +
                                                             "\n" +
                                                             "\nЧем быстрее будет получен верный ответ, тем выше будет награда. За серию верных ответов награда увеличивается многократно. " +
                                                             "Если будет заминка в ответах, я дам подсказку. Если уж совсем никто не угадает... ну, не беда. " +
                                                             "\n" +
                                                             "\nВопросы будут следовать один за другим с разницей в некоторое время - вы успеете посмотреть таблицу лидеров (команда \"топ!\"), " +
                                                             "запросить информацию по своим результатам (команда \"я!\"), или просто поболтать." +
                                                             "\n" +
                                                             "\nВсё очень просто. Удачи.");
                            return Task.CompletedTask;
                        }

                    #endregion

                    #region Команды для управленцев в канале управления

                    // загрузить новый пак вопросов
                    case "новый-пак":
                    case "пак":
                        {
                            if (SettingsManager.CurrentSettings[SettingsNames.DiscordAdministratorsIds]
                                .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .All(x => ulong.Parse(x) != message.Author.Id))
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые вопросы: недостаточно прав.");
                                return Task.CompletedTask;
                            }

                            if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые вопросы: этот канал не предназначен для управления ботом.");
                                return Task.CompletedTask;
                            }

                            if (_gameManager.IsActive)
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые вопросы: викторина запущена.");
                                return Task.CompletedTask;
                            }

                            var attachment = message.Attachments.FirstOrDefault();

                            if (attachment == null)
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые вопросы: не приложен файл.");
                                return Task.CompletedTask;
                            }

                            using var client = new WebClient();
                            var attachmentPath = Path.Combine(AppContext.BaseDirectory, "App_Data", "newQuizzes.btp");
                            client.DownloadFile(new Uri(attachment.Url), attachmentPath);

                            try
                            {
                                var deleteOld = content.Contains("новый");
                                message.Channel.SendMessageAsync("Начат процесс загрузки новых вопросов." + (deleteOld ? " Предыдущий пак будет удалён." : null));
                                var result = _gameManager.AddQuizzesPack(attachmentPath, deleteOld);
                                message.Channel.SendMessageAsync("В базу данных загружен новый пак из " + result + " шт. вопросов." + (deleteOld ? " Предыдущий пак был удалён." : null));
                            }
                            catch (BuktopuhaException e)
                            {
                                message.Channel.SendMessageAsync("Не удалось загрузить данные в базу. " + e.Message);
                            }

                            break;
                        }
                    // перемешать все вопросы в базе данных
                    case "перемешать":
                        {
                            if (SettingsManager.CurrentSettings[SettingsNames.DiscordAdministratorsIds]
                                .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .All(x => ulong.Parse(x) != message.Author.Id))
                            {
                                message.Channel.SendMessageAsync("Невозможно перемешать вопросы в базе данных: недостаточно прав.");
                                return Task.CompletedTask;
                            }

                            if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                            {
                                message.Channel.SendMessageAsync("Невозможно перемешать вопросы в базе данных: этот канал не предназначен для управления ботом.");
                                return Task.CompletedTask;
                            }

                            if (_gameManager.IsActive)
                            {
                                message.Channel.SendMessageAsync("Невозможно перемешать вопросы в базе данных: викторина запущена.");
                                return Task.CompletedTask;
                            }

                            try
                            {
                                DataBaseManager.ShuffleAllQuizzesInDataBase();
                                message.Channel.SendMessageAsync("Вопросы в базе данных были успешно перемешаны.");
                            }
                            catch (Exception e)
                            {
                                message.Channel.SendMessageAsync("Не удалось перемешать вопросы в базе данных. " + e.Message);
                            }

                            break;
                        }
                    // сбросить все данные в базе
                    case "тотальный-сброс":
                        {
                            if (SettingsManager.CurrentSettings[SettingsNames.DiscordAdministratorsIds]
                                .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .All(x => ulong.Parse(x) != message.Author.Id))
                            {
                                message.Channel.SendMessageAsync("Невозможно выполнить сброс базы данных: недостаточно прав.");
                                return Task.CompletedTask;
                            }

                            if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                            {
                                message.Channel.SendMessageAsync("Невозможно выполнить сброс базы данных: этот канал не предназначен для управления ботом.");
                                return Task.CompletedTask;
                            }

                            if (_gameManager.IsActive)
                            {
                                message.Channel.SendMessageAsync("Невозможно выполнить сброс базы данных: викторина запущена.");
                                return Task.CompletedTask;
                            }

                            try
                            {
                                _gameManager.ClearAll();
                                message.Channel.SendMessageAsync("Тотальная очистка базы данных бота проведена.");
                            }
                            catch (BuktopuhaException e)
                            {
                                message.Channel.SendMessageAsync("Не удалось выполнить сброс базы данных. " + e.Message);
                            }

                            break;
                        }
                    // обновить настройки
                    case "настройки":
                        {
                            if (SettingsManager.CurrentSettings[SettingsNames.DiscordAdministratorsIds]
                                .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .All(x => ulong.Parse(x) != message.Author.Id))
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые настройки: недостаточно прав.");
                                return Task.CompletedTask;
                            }

                            if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые настройки: этот канал не предназначен для управления ботом.");
                                return Task.CompletedTask;
                            }
                            if (_gameManager.IsActive)
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые настройки: викторина запущена.");
                                return Task.CompletedTask;
                            }

                            var attachment = message.Attachments.FirstOrDefault();

                            if (attachment == null)
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые настройки: не приложен файл.");
                                return Task.CompletedTask;
                            }

                            var webStream = new WebClient().OpenRead(attachment.Url);
                            if (webStream == null)
                            {
                                message.Channel.SendMessageAsync(
                                    "Новые вопросы не настройки. Не удалось загрузить вложенный файл.");
                                return Task.CompletedTask;
                            }

                            var fileContentPath = Path.Combine(AppContext.BaseDirectory, "App_Data", "newSettings.ini");
                            using (var reader = new StreamReader(webStream))
                            {
                                File.WriteAllText(fileContentPath, reader.ReadToEnd());
                            }

                            try
                            {
                                message.Channel.SendMessageAsync("Загрузка настроек.");
                                var newSettings = File.ReadAllLines(fileContentPath)
                                    .Select(x => x.Split('='))
                                    .Where(x => x.Length == 2)
                                    .ToDictionary(x => Enum.Parse<SettingsNames>(x[0].Trim()), x => x[1].Trim());
                                SettingsManager.ChangeAndSaveSetting(newSettings);
                                message.Channel.SendMessageAsync("Новые настройки загружены в количестве " +
                                                                 newSettings.Count + " шт.");
                            }
                            catch (Exception e)
                            {
                                message.Channel.SendMessageAsync("Невозможно загрузить новые настройки: ошибка форматирования файла.\n"
                                                                 + e.Message);
                                return Task.CompletedTask;
                            }

                            return Task.CompletedTask;
                        }
                    // узнать, сколько предложений есть в базе данных
                    case "предложения-сколько":
                        {
                            if (SettingsManager.CurrentSettings[SettingsNames.DiscordAdministratorsIds]
                                .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .All(x => ulong.Parse(x) != message.Author.Id))
                            {
                                message.Channel.SendMessageAsync("Невозможно работать с предложениями: недостаточно прав.");
                                return Task.CompletedTask;
                            }

                            if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                            {
                                message.Channel.SendMessageAsync("Невозможно работать с предложениями: этот канал не предназначен для управления ботом.");
                                return Task.CompletedTask;
                            }

                            message.Channel.SendMessageAsync("Количество предложений в базе данных: " +
                                                             DataBaseManager.SuggestionsCount());

                            break;
                        }
                    // получить 10 номеров предложений из базы данных
                    case "предложения-номера":
                        {
                            if (SettingsManager.CurrentSettings[SettingsNames.DiscordAdministratorsIds]
                                .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                                .All(x => ulong.Parse(x) != message.Author.Id))
                            {
                                message.Channel.SendMessageAsync("Невозможно работать с предложениями: недостаточно прав.");
                                return Task.CompletedTask;
                            }

                            if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                            {
                                message.Channel.SendMessageAsync("Невозможно работать с предложениями: этот канал не предназначен для управления ботом.");
                                return Task.CompletedTask;
                            }


                            message.Channel.SendMessageAsync("Номера предложений: " +
                                                             DataBaseManager.GetSuggestionsIds(10)
                                                                 .Select(x => "№ " + x)
                                                                 .Aggregate((x, y) => x + ", " + y));

                            break;
                        }
                    #endregion

                    default:
                        // работа с именем пользователя
                        if (message.Content.ToLower().StartsWith("имя"))
                        {
                            content = message.Content["имя".Length..^1].TrimStart(' ', '=');
                            if (string.IsNullOrWhiteSpace(content))
                            {
                                message.Channel.SendMessageAsync("Невозможно изменить имя игрока: новое имя не найдено после командного " +
                                                                 "слова \"имя \" до восклицательного знака.");
                                return Task.CompletedTask;
                            }

                            var (result, err) = DataBaseManager.UpdatePlayerName(message.Author.Id, content.Trim('=', ' '));
                            message.Channel.SendMessageAsync(result
                                ? $"Имя {message.Author.Mention} успешно изменено на {content}."
                                : $"Имя {message.Author.Mention} не было изменено. " + err);
                            return Task.CompletedTask;
                        }

                        // если ни одна из команд: воспринимает это как попытку угадать
                        break;
                }
            }
            // предлагает вопрос для викторины
            else if (message.Content.ToLower().StartsWith("вопрос!"))
            {
                if (message.Attachments.Any())
                {
                    var mainDir = Path.Combine(AppContext.BaseDirectory, "App_Data", "tempSuggestions");
                    if (Directory.Exists(mainDir))
                        Directory.Delete(mainDir, true);
                    Directory.CreateDirectory(mainDir);
                    var contentDir = Path.Combine(mainDir, "content");
                    Directory.CreateDirectory(contentDir);
                    foreach (var attachment in message.Attachments)
                    {
                        var path = Path.Combine(contentDir, attachment.Filename);
                        using var client = new WebClient();
                        client.DownloadFile(new Uri(attachment.Url), path);
                    }

                    var zipFilePath = Path.Combine(mainDir, "attachments.zip");

                    ZipFile.CreateFromDirectory(contentDir, zipFilePath);
                    AddSuggestion(zipFilePath);
                    if (Directory.Exists(mainDir))
                        Directory.Delete(mainDir, true);
                }
                else
                {
                    AddSuggestion();
                }

                void AddSuggestion(string zipPath = null)
                {
                    DataBaseManager.SuggestNewQuiz
                    (
                        $"Автор: {message.Author.Username} | {message.Author.Id}" + Environment.NewLine +
                        $"Дата: {DateTime.Now:O}" + Environment.NewLine +
                        message.Content,
                        zipPath is null || !File.Exists(zipPath) ? null : File.ReadAllBytes(zipPath)
                    );
                    message.Channel.SendMessageAsync($"{message.Author.Mention}, вопрос был принят и появится " +
                                                     "в викторине, если и когда пройдёт предварительную модерацию.");
                    message.DeleteAsync();
                }
            }
            // управление предложением по его номеру
            else if (message.Content.ToLower().StartsWith("предложение!"))
            {
                if (SettingsManager.CurrentSettings[SettingsNames.DiscordAdministratorsIds]
                    .Split(new[] { ',', ';', ' ' }, StringSplitOptions.RemoveEmptyEntries)
                    .All(x => ulong.Parse(x) != message.Author.Id))
                {
                    message.Channel.SendMessageAsync("Невозможно работать с предложениями: недостаточно прав.");
                    return Task.CompletedTask;
                }

                if (message.Channel.Id != ulong.Parse(SettingsManager.CurrentSettings[SettingsNames.DiscordManageChannelId]))
                {
                    message.Channel.SendMessageAsync("Невозможно работать с предложениями: этот канал не предназначен для управления ботом.");
                    return Task.CompletedTask;
                }

                int id;
                // команда к удалению предложения из базы
                if (message.Content.ToLower().StartsWith("предложение!удалить-"))
                {
                    content = message.Content.ToLower()["предложение!удалить-".Length..];
                    if (!int.TryParse(content, out id))
                    {
                        message.Channel.SendMessageAsync(
                            "Невозможно удалить сообщение: номер не распознан");
                        return Task.CompletedTask;
                    }

                    DataBaseManager.DeleteSuggestion(id);
                    message.Channel.SendMessageAsync($"Предложение с номером {id} было удалено из базы");
                    return Task.CompletedTask;
                }

                // а тут надо достать предложение из базы
                content = message.Content.ToLower()["предложение!".Length..];
                if (!int.TryParse(content, out id))
                {
                    message.Channel.SendMessageAsync(
                        "Невозможно получить сообщение: номер не распознан");
                    return Task.CompletedTask;
                }

                // вопрос без вложений
                var entity = DataBaseManager.GetSuggestion(id);
                if (entity.AttachmentsZip == null || entity.AttachmentsZip.Length < 1)
                {
                    message.Channel.SendMessageAsync(entity.TextInformation);
                    return Task.CompletedTask;
                }

                var tempZip = Path.Combine(AppContext.BaseDirectory, $"TempGetSuggestion{id}.zip");
                if (File.Exists(tempZip))
                    File.Delete(tempZip);
                File.WriteAllBytes(tempZip, entity.AttachmentsZip);

                message.Channel.SendFileAsync(tempZip, entity.TextInformation);
            }


            // Здесь обрабатывается попытки игроков угадать
            if (!_gameManager.IsActive)
                return Task.CompletedTask;

            lock (_gameManager.QueueLocker)
            {
                if (!_gameManager.AnyGuessesFlag)
                    _gameManager.AnyGuessesFlag = true;
                _gameManager.CurrentQuiz.Guesses.Enqueue(new UserGuess
                {
                    Guess = content,
                    DiscordUserName = message.Author.Username,
                    UserId = message.Author.Id
                });
            }

            return Task.CompletedTask;

        }

    }
}
