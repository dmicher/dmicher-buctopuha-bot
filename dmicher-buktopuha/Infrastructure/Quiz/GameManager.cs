﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Discord.WebSocket;
using dmicher_buktopuha.Infrastructure.DataBase;
using dmicher_buktopuha.Infrastructure.Settings;

namespace dmicher_buktopuha.Infrastructure.Quiz
{
    /// <summary>
    /// Объект, управляющий викториной
    /// </summary>
    internal class GameManager
    {
        #region Свойства и поля

        /// <summary>
        /// Очередь вопросов
        /// </summary>
        private Queue<Quiz> Quizzes { get; } = new();

        /// <summary>
        /// Текущий вопрос
        /// </summary>
        internal Quiz CurrentQuiz { get; private set; }

        /// <summary>
        /// Статус активности вопросника
        /// </summary>
        internal bool IsActive { get; private set; }

        /// <summary>
        /// Блокировщик очереди
        /// </summary>
        internal object QueueLocker { get; } = new();

        /// <summary>
        /// Блокировщик флага остановки
        /// </summary>
        internal object StopFlagLocker { get; } = new();

        /// <summary>
        /// Флаг того, что после этого вопроса нужно остановить викторину
        /// </summary>
        internal bool StopFlag { get; set; }

        /// <summary>
        /// Флаг того, что за последний вопрос хоть кто-то пытался угадать
        /// </summary>
        internal bool AnyGuessesFlag { get; set; }

        /// <summary>
        /// Время в секундах, через которое будет показана подсказка
        /// </summary>
        private TimeSpan _hintTimeSpan;

        /// <summary>
        /// Время в секундах, выделенное на угадывание вопроса
        /// </summary>
        private TimeSpan _totalQuestionTimeSpan;

        /// <summary>
        /// Время в секундах между вопросами
        /// </summary>
        private int _sleepBetweenQuestionsSeconds;

        /// <summary>
        /// Критически низкое количество оставшихся вопросов в очереди,
        /// когда пора загружать новые вопросы из базы данных
        /// </summary>
        private int _quizzesLoadWhenRemain;

        /// <summary>
        /// Сколько вопросов из базы данных загружать за раз
        /// </summary>
        private int _quizzesLoadCount;

        /// <summary>
        /// Количество наиболее старых вопросов, загружаемых в выборку (в процентах)
        /// </summary>
        private byte _quizzesLoadPercentOfOld;

        /// <summary>
        /// Название очков опыта
        /// </summary>
        private NumericName _playersExpName;

        /// <summary>
        /// Длина победной серии, на которой действует модификатор первого уровня,
        /// увеличивающий получаемый опыт 
        /// </summary>
        private uint _playersWinnerStreakLevel1;

        /// <summary>
        /// Модификатор опыта первого уровня
        /// </summary>
        private uint _playersWinnerStreakScoreFactor1;

        /// <summary>
        /// Длина победной серии, на которой действует модификатор второго уровня,
        /// увеличивающий получаемый опыт 
        /// </summary>
        private uint _playersWinnerStreakLevel2;

        /// <summary>
        /// Модификатор опыта второго уровня
        /// </summary>
        private uint _playersWinnerStreakScoreFactor2;

        /// <summary>
        /// Длина победной серии, на которой действует модификатор третьего уровня,
        /// увеличивающий получаемый опыт 
        /// </summary>
        private uint _playersWinnerStreakLevel3;

        /// <summary>
        /// Модификатор опыта третьего уровня
        /// </summary>
        private uint _playersWinnerStreakScoreFactor3;

        /// <summary>
        /// Отображать ли верные ответы, если ответ не отгадан
        /// </summary>
        private bool _quizShowAnswerWhenLoose;

        /// <summary>
        /// Показывать ли альтернативные ответы вместе с основным после завершения вопроса
        /// </summary>
        private bool _quizShowAlternativeAnswers;

        /// <summary>
        /// Показывать ли авторов при задавании вопроса
        /// </summary>
        private bool _quizShowAuthors;

        /// <summary>
        /// Период вывода таблицы лидеров после вопросов
        /// </summary>
        private int _leadersTableRepeatPeriod;

        /// <summary>
        /// Режим остановки игры:
        /// 0 - по общему количеству вопросов,
        /// 1 - по количеству вопросов, на которые не было попыток ответить,
        /// 2 - только ручное отключение.
        /// </summary>
        private byte _gameStopMode;

        /// <summary>
        /// Количество вопросов (в зависимости от <see cref="_gameStopMode"/>):
        /// 0 - всего вопросов в серии;
        /// 1 - вопросов без ответов;
        /// 2 - не используется.
        /// </summary>
        private uint _gameStopQuizzesCount;

        /// <summary>
        /// Канал, в который отправляются сообщения
        /// </summary>
        private readonly SocketTextChannel _channel;

        /// <summary>
        /// Идентификатор Discord последнего верно ответившего игрока
        /// </summary>
        private ulong LastWinnerId { get; set; }

        /// <summary>
        /// Победная серия последнего ответившего игрока
        /// </summary>
        private uint LastWinnerStreak { get; set; }

        /// <summary>
        /// Количество вопросов, в зависимости от выбранного метода остановки:
        /// а. оставшихся до конца серии вопросов
        /// б. проигнорированных пользователями (без единой попытки ответить)
        /// </summary>
        private uint _currentQuizzesCount;

        /// <summary>
        /// Режим добавления вопросов:
        /// 0 - случайная выборка с приоритетом тех вопросов, которые давно не использовались;
        /// 1 - в порядке, указанном в базе данных, начиная с тех, которые использовались меньше всего.
        /// </summary>
        private byte _gameAddQuizzesMode;

        #endregion

        /// <summary>
        /// Создаёт объект обработки вопросов
        /// </summary>
        /// <param name="client">Клиент Discord для отправки сообщений</param>
        public GameManager(DiscordSocketClient client)
        {
            var settings = SettingsManager.CurrentSettings;
            var guid = client.GetGuild(ulong.Parse(settings[SettingsNames.DiscordQuizzesGuildId]));
            _channel = guid.GetTextChannel(ulong.Parse(settings[SettingsNames.DiscordQuizzesChannelId]));
        }

        /// <summary>
        /// Запускает вопросник, если он ещё не запущен
        /// </summary>
        internal void Start()
        {
            if (IsActive)
                return;

            var settings = SettingsManager.CurrentSettings;

            _hintTimeSpan = new TimeSpan(0, 0, int.Parse(settings[SettingsNames.QuizHintTimeSpan]));
            _totalQuestionTimeSpan = new TimeSpan(0, 0, int.Parse(settings[SettingsNames.QuizQuestionTimeSpan]));
            _sleepBetweenQuestionsSeconds = int.Parse(settings[SettingsNames.QuizSleepBetweenQuestions]);
            _quizzesLoadWhenRemain = int.Parse(settings[SettingsNames.QuizzesLoadWhenRemain]);
            _quizzesLoadCount = int.Parse(settings[SettingsNames.QuizzesLoadCount]);
            var expNames = SettingsManager.CurrentSettings[SettingsNames.PlayersExpName].Split(',');
            _playersExpName = new NumericName(expNames[0], expNames[1], expNames[2]);
            _playersWinnerStreakLevel1 = uint.Parse(settings[SettingsNames.PlayersWinnerStreakLevel1]);
            _playersWinnerStreakScoreFactor1 = uint.Parse(settings[SettingsNames.PlayersWinnerStreakScoreFactor1]);
            _playersWinnerStreakLevel2 = uint.Parse(settings[SettingsNames.PlayersWinnerStreakLevel2]);
            _playersWinnerStreakScoreFactor2 = uint.Parse(settings[SettingsNames.PlayersWinnerStreakScoreFactor2]);
            _playersWinnerStreakLevel3 = uint.Parse(settings[SettingsNames.PlayersWinnerStreakLevel3]);
            _playersWinnerStreakScoreFactor3 = uint.Parse(settings[SettingsNames.PlayersWinnerStreakScoreFactor3]);
            _quizShowAnswerWhenLoose = bool.Parse(settings[SettingsNames.QuizShowAnswerWhenLoose]);
            _quizShowAlternativeAnswers = bool.Parse(settings[SettingsNames.QuizShowAlternativeAnswers]);
            _quizzesLoadPercentOfOld = byte.Parse(settings[SettingsNames.QuizzesLoadPercentOfOld]);
            _quizShowAuthors = bool.Parse(settings[SettingsNames.QuizShowAuthors]);
            _leadersTableRepeatPeriod = int.Parse(settings[SettingsNames.LeadersTableRepeatPeriod]);
            _gameStopMode = byte.Parse(settings[SettingsNames.GameStopMode]);
            _gameStopQuizzesCount = uint.Parse(settings[SettingsNames.GameStopQuizzesCount]);
            _currentQuizzesCount = _gameStopMode == 0
                ? _gameStopQuizzesCount // по общему количеству вопросов: количество вопросов для этой сессии
                : 0; // по игнорируемым вопросам: ни одного проигнорированного вопроса
            _gameAddQuizzesMode = byte.Parse(settings[SettingsNames.GameAddQuizzesMode]);

            DataBaseManager.PrepareCleanImageDirPath(Quizzes.Where(x => x.HasImage).Select(x => x.ImagePath).ToArray());
            if (!Quizzes.Any()) LoadQuizzes();

            var quizThread = new Thread(RunQuiz);
            quizThread.Start();
        }

        /// <summary>
        /// Выполнение этого запускается в новом потоке.
        /// </summary>
        private void RunQuiz()
        {
            IsActive = true;    // отмечает, что вопросы запущены
            StopFlag = false;   // спускает флаг остановки
            var currentRunFromLastTopTableShow = _leadersTableRepeatPeriod; // количество вопросов с последнего автоматического вывода топа игроков

            var cycleStartWords = SettingsManager.CurrentSettings[SettingsNames.QuizGreetings] // сообщение о старте или (будет заполнено ниже) верном ответе игрока
                .Replace("\\n", "\n").Replace("\\t", "    ")
                .Replace("BotId", SettingsManager.CurrentSettings[SettingsNames.DiscordBotId]) +
                                  (_gameStopMode == 0
                                      ? "Играем серию вопросов в количестве " + _currentQuizzesCount + "шт."
                                      : "Играем, пока вы отвечаете на вопросы.");

            do
            {
                _channel.SendMessageAsync(cycleStartWords + $"\nСледующий вопрос через **{_sleepBetweenQuestionsSeconds} сек.**");

                // выводит сообщение о лучших игроках каждые несколько вопросов (по настройке)
                if (currentRunFromLastTopTableShow++ >= _leadersTableRepeatPeriod)
                {
                    var (success, result) = Top();
                    if (success)
                        _channel.SendMessageAsync("\n**Топ игроков:**" + result);
                    currentRunFromLastTopTableShow = 0;
                }

                // добавляет вопросы, если те подходят к концу
                Thread.Sleep(100);
                if (Quizzes.Count < _quizzesLoadWhenRemain)
                    Task.Run(() => // задача в отдельном потоке на добавление вопросов
                    {
                        try
                        {
                            LoadQuizzes();
                        }
                        catch (BuktopuhaException e)
                        {
                            _channel.SendMessageAsync("Ошибка загрузки новой серии вопросов. " + e.Message);
                        }
                    });

                // ждёт период между вопросами, после чего вытаскивает вопрос из очереди
                Thread.Sleep(_sleepBetweenQuestionsSeconds * 1000);
                lock (QueueLocker)
                {
                    do
                    {
                        CurrentQuiz = Quizzes.Dequeue();
                        if (CurrentQuiz.HasImage && !File.Exists(CurrentQuiz.ImagePath))
                            continue;
                        Thread.Sleep(100);
                    } while (CurrentQuiz.State == Quiz.QuizState.Stopped); // вытаскивает только валидный вопрос
                }

                // обнуляет флаг наличия ответов и готовит текст следующего вопроса
                AnyGuessesFlag = false;
                var message = "Следующий вопрос" +
                              (!_quizShowAuthors || string.IsNullOrWhiteSpace(CurrentQuiz.Author)
                                  ? null
                                  : $" от **{CurrentQuiz.Author}**") + "!" +
                              "\n\n**" + CurrentQuiz.Start() +
                              "**\n\n*Ответ: состоит из [" +
                              CurrentQuiz.MainAnswer
                                  .Split(Quiz.SpaceChars, StringSplitOptions.RemoveEmptyEntries)
                                  .Select(x => x.Length.ToString()).Aggregate((x, y) => x + "-" + y)
                              + "] букв" +
                              (CurrentQuiz.AnswerSpaceChars is { Length: > 0 }
                                ? $", имеет знаки [{CurrentQuiz.AnswerSpaceChars.Select(x => x.ToString()).Aggregate((x, y) => x + ", " + y)}]"
                                : null) +
                              (CurrentQuiz.IsMainAnswerANumber
                                  ? ", это число"
                                  : CurrentQuiz.MainAnswerContainsNumber
                                      ? ", содержит число"
                                      : null) +
                              (CurrentQuiz.IsMainAnswerInLatin
                                  ? ", на латинице"
                                  : null) +
                              (CurrentQuiz.AlternativeAnswers?.Any() ?? false
                                  ? $", альтернатив {CurrentQuiz.AlternativeAnswers.Count()}"
                                  : null) +
                              $", подсказок {CurrentQuiz.HintsCount}.*";

                // в зависимости от того, есть ли в вопросе картинка вызывает соответствующий
                // метод публикации вопроса и отправляет вопрос игрокам
                if (CurrentQuiz.HasImage)
                {
                    // отправка вопроса с картинкой - это отправка файла с описанием
                    _channel.SendFileAsync(CurrentQuiz.ImagePath, message);
                    var _ = new Timer( // таймер, удаляющий картинку вопроса из временной папки через минуту
                        o =>
                        {
                            var path = (string)o;
                            if (string.IsNullOrWhiteSpace(path))
                                return;
                            if (File.Exists(path))
                                File.Delete(path);
                        },
                        new string(CurrentQuiz.ImagePath),
                        new TimeSpan(0, 1, 0),
                        new TimeSpan()
                    );
                }
                else
                {
                    // отправка простого текстового вопроса
                    _channel.SendMessageAsync(message);
                }

                Thread.Sleep(100);

                // ожидает верного ответа игроков или истечения времени на ответ (что наступит раньше)
                ulong winnerId;
                string winnerName;
                uint winnerScore;
                do
                {
                    Thread.Sleep(500);
                    if (DateTime.Now - CurrentQuiz.LastHintTime > _hintTimeSpan && CurrentQuiz.IsHintsRemaining)
                    {
                        _channel.SendMessageAsync("Подсказка: " + CurrentQuiz.NextHint);
                        Thread.Sleep(100);
                    }
                    (winnerId, winnerName, winnerScore) = CurrentQuiz.CheckWinner((uint)_totalQuestionTimeSpan.TotalSeconds);
                } while (winnerId == 0 && DateTime.Now - CurrentQuiz.StartTime < _totalQuestionTimeSpan);

                // завершение работы с текущим вопросом
                CurrentQuiz.State = Quiz.QuizState.Stopped;

                // ЕСЛИ НА ВОПРОС ПРАВИЛЬНО ОТВЕТИЛ ОДИН ИЗ ИГРОКОВ
                if (winnerId > 0)
                {
                    // определяет продолжительность серии победителя (добавляет единицу для победителя, отвечающего под ряд,
                    // а, если ответил другой игрок, сбрасывает до 1, запоминая нового ответившего игрока)
                    if (LastWinnerId == winnerId)
                    {
                        LastWinnerStreak++;
                    }
                    else
                    {
                        LastWinnerId = winnerId;
                        LastWinnerStreak = 1;
                    }

                    // определяет модификатор опыта, который получит игрок за серию ответов
                    var multiplication = LastWinnerStreak >= _playersWinnerStreakLevel3
                        ? _playersWinnerStreakScoreFactor3
                        : LastWinnerStreak >= _playersWinnerStreakLevel2
                            ? _playersWinnerStreakScoreFactor2
                            : LastWinnerStreak >= _playersWinnerStreakLevel1
                                ? _playersWinnerStreakScoreFactor1
                                : 1;

                    // определяет информацию о выигрышном ответе
                    var totalScore = winnerScore * multiplication;
                    var ans = new NumericName("ответа", "ответов", "ответов"); // из 1 ответа, из 3 ответов, из 5 ответов
                    var pos = new NumericName("позицию", "позиции", "позиций"); // на 1 позицию, на 3 позиции, на 5 позиций
                    var positionBefore = DataBaseManager.GetPosition(winnerId).position;
                    winnerName = DataBaseManager.AddScoreAndGetName(winnerId, totalScore, LastWinnerStreak, winnerName);
                    var (positionNow, sameScoreNow) = DataBaseManager.GetPosition(winnerId);
                    var positionDiff = positionBefore - positionNow;

                    // изменяет счётчик вопросов
                    if (_gameStopMode == 0)
                    {
                        // режим "общее количество" - уменьшает счётчик на единицу (вопрос сыгран)
                        // и, если счётчик достиг нуля, поднимает флаг завершения, чтобы закончить серию вопросов
                        StopFlag |= --_currentQuizzesCount < 1;
                    }
                    else
                    {
                        // режим "проигнорированные вопросы" - сбрасывает счётчик (вопрос не проигнорирован)
                        _currentQuizzesCount = 0;
                    }

                    // заполняет начало сообщения для вывода, чтобы показать информацию о выигрыше игрокам (будет выведено в следующем цикле)
                    cycleStartWords = $"**Первым угадал {winnerName} [<@{winnerId}>] **!\n*Правильный ответ: {CurrentQuiz.MainAnswer}" +
                          (_quizShowAlternativeAnswers && (CurrentQuiz.AlternativeAnswers?.Any() ?? false)
                            ? $", или же: {CurrentQuiz.AlternativeAnswers.Aggregate((x, y) => x + ", " + y)}"
                            : null
                          ) +
                          ".*\n" +
                          $"*За серию из {LastWinnerStreak} {ans.GetStringByNumber(LastWinnerStreak)} он получает " +
                          $" {winnerScore} × {multiplication} = {totalScore} {_playersExpName.GetStringByNumber(totalScore)}.\n" +
                          (positionDiff == 0
                            ? $"Он занимает {positionNow} строчку таблицы лидеров"
                            : $"Он поднялся на {positionDiff} {pos.GetStringByNumber(positionDiff)} вверх " +
                              $"и занимает теперь {positionNow} строчку топа"
                          ) +
                          (sameScoreNow == 0
                            ? ".*"
                            : $" (игроков с тем же счётом - {sameScoreNow}).*"
                          ) +
                          (_gameStopMode == 0
                              ? "\n\n*Осталось вопросов: **" + _currentQuizzesCount + "** .*"
                              : null);

                    continue; // запускает новый вопрос в серии
                }

                // ЕСЛИ НА ВОПРОС НИКТО ВЕРНО НЕ ОТВЕТИЛ
                // обнуляет счетчик серии побед
                LastWinnerId = 0;
                LastWinnerStreak = 0;

                // изменяет счётчик вопросов в зависимости от режима остановки и проверяет условия выхода из серии вопросов
                if (_gameStopMode == 0)
                {
                    // по общему количеству вопросов: уменьшает остаток вопросов и,
                    // если количество равно нулю, поднимает флаг остановки
                    StopFlag |= --_currentQuizzesCount < 1;
                }
                else
                {
                    // по наличию попыток ответить на вопрос
                    if (AnyGuessesFlag)
                    {
                        // если были попытки ответов, сбрасывает счётчик до нуля (вопрос не игнорировался игроками)
                        _currentQuizzesCount = 0;
                    }
                    else
                    {
                        // если никто не угадывал, увеличивает счётчик, а при выходе за объявленный предел поднимает флаг остановки
                        StopFlag |= ++_currentQuizzesCount > _gameStopQuizzesCount;
                    }
                }

                // заполняет начало сообщения для вывода, чтобы показать информацию о том, что никто не угадал (будет выведено в следующем цикле)
                cycleStartWords = "Увы, **никто не угадал** ответ." +
                                  (AnyGuessesFlag || _gameStopMode != 1
                                      ? null // если кто-либо угадал или режим игры НЕ "остановка по проигнорированным вопросам" - не комментирует
                                      : _currentQuizzesCount switch // если режим "остановка при игнорировании" - предупреждает едким комментарием
                                      {
                                          1 => " Кажется никто даже и не пытался!",
                                          2 => " Второй раз под ряд! Люди! Ау!",
                                          3 => " Третий вопрос в пустоту... где же игроки?",
                                          4 => " Люди, это уже не смешно: я тут ради кого стараюсь?",
                                          5 => " Игнорируете? Ну и ладно...",
                                          _ => "\n*...(обиделся и готовится прекратить игру)...*"
                                      }) +
                                  (_quizShowAnswerWhenLoose
                                      ? $"\n*Правильный ответ: {CurrentQuiz.MainAnswer}" +
                                        (_quizShowAlternativeAnswers && (CurrentQuiz.AlternativeAnswers?.Any() ?? false)
                                            ? $", или же: {CurrentQuiz.AlternativeAnswers.Aggregate((x, y) => x + ", " + y)}"
                                            : null) + ".*"
                                      : null) +
                                  (_gameStopMode == 0
                                      ? " Остаток вопросов в серии: " + _currentQuizzesCount
                                      : " Если никто не будет играть, обижусь и остановлюсь.");
            } while (!StopFlag); // серия вопросов продолжается, пока флаг остановки не будет поднят (изнутри или извне)

            // завершение серии вопросов

            var (endTopSuccess, endTopResult) = Top();
            _channel.SendMessageAsync(cycleStartWords + "\n\n**Игра окончена.** Спасибо за ваши ответы." +
                                      (endTopSuccess ? "\n**Топ игроков:**" + endTopResult : null));
            Thread.Sleep(100);

            IsActive = false;
        }

        /// <summary>
        /// Загружает новые элементы в игровую очередь
        /// </summary>
        private void LoadQuizzes()
        {
            // при самой первой загрузке сначала подгружает информацию из настроек
            if (_quizzesLoadCount == 0)
                _quizzesLoadCount = int.Parse(SettingsManager.CurrentSettings[SettingsNames.QuizzesLoadCount]);
            if (_quizzesLoadPercentOfOld == 0)
                _quizzesLoadPercentOfOld = byte.Parse(SettingsManager.CurrentSettings[SettingsNames.QuizzesLoadPercentOfOld]);

            // получает вопросы из базы данных
            var newQuizzes = _gameAddQuizzesMode == 0
                ? DataBaseManager.GetRandomQuizzes(_quizzesLoadCount, _quizzesLoadPercentOfOld)?.ToArray()
                : DataBaseManager.GetNextQuizzes(_quizzesLoadCount)?.ToArray();
            
            if (newQuizzes == null || !newQuizzes.Any())
            {
                throw new BuktopuhaException("В базе данных отсутствуют вопросы для викторины.");
            }

            // загружает вопросы в очередь
            lock (QueueLocker)
            {
                foreach (var quiz in newQuizzes) Quizzes.Enqueue(quiz);
            }
        }

        /// <summary>
        /// Загружает все вопросы из файла, заменяя ими все вопросы в базе данных.
        /// </summary>
        /// <param name="filePath">Путь к загруженному файлу с задачами</param>
        /// <param name="deleteOldQuizzes">Удалять ли все ранее загруженные вопросы?</param>
        internal int AddQuizzesPack(string filePath, bool deleteOldQuizzes)
        {
            // распаковывает файл вопросов во временную папку
            var tempDir = Path.Combine(AppContext.BaseDirectory, "App_Data", "temp_dir");
            if (Directory.Exists(tempDir))
                Directory.Delete(tempDir, true);
            Directory.CreateDirectory(tempDir);

            ZipFile.ExtractToDirectory(filePath, tempDir);

            // добывает из распакованного файла вопросы
            var contentFilePath = Path.Combine(tempDir, "quizzes.xml");
            if (!File.Exists(contentFilePath))
                throw new Exception("Невозможно применить пак новых вопросов: неверный формат файла - отсутствует компонента вопросов.");

            QuizEntity[] quizzes;
            using (var stream = File.OpenRead(contentFilePath))
            {
                var serializer = new XmlSerializer(typeof(QuizEntity[]));
                quizzes = (QuizEntity[])serializer.Deserialize(stream);
            }

            if (quizzes == null || quizzes.Length == 0)
            {
                _channel.SendMessageAsync("Невозможно применить пак новых вопросов: компонента вопросов не имеет должной информации.");
                return -1;
            }

            // дополнительно обрабатывает вопросы, содержащие картинки
            var imageQuestions = quizzes.Where(x => x.HasImage).ToArray();
            var images = new List<ImageEntity>();
            if (imageQuestions.Length > 0)
            {
                var imageDir = Path.Combine(tempDir, "images");
                if (!Directory.Exists(imageDir))
                    throw new Exception("Невозможно применить пак новых вопросов: отсутствует компонента изображений.");
                var imageFiles = Directory.GetFiles(imageDir);

                foreach (var entity in imageQuestions)
                {
                    var imagePath = imageFiles.FirstOrDefault(x => x.Contains(entity.ImageGuid.ToString()));
                    if (string.IsNullOrWhiteSpace(imagePath))
                        throw new Exception("Невозможно применить пак новых вопросов: компонента изображений содержит не все необходимые файлы.");

                    images.Add(new ImageEntity
                    { Name = Path.GetFileName(imagePath), Picture = File.ReadAllBytes(imagePath) });
                }
            }

            // готовит массив вопросов к вставке в базу данных
            var rand = new Random(Environment.TickCount);
            for (var runNumber = 0; runNumber < 10; runNumber++)
            {
                // несколько раз перемешивает массив, чтобы добиться более-менее случайного распределения вопросов
                // (тематически похожие вопросы могут быть написаны одним автором друг рядом с другом - этого следует избегать)
                for (var startIndex = quizzes.Length - 1; startIndex >= 1; startIndex--)
                {
                    var endIndex = rand.Next(startIndex + 1);
                    (quizzes[endIndex], quizzes[startIndex]) = (quizzes[startIndex], quizzes[endIndex]);
                    if (runNumber > 0) continue;

                    // при первом перемешивании обнуляет номера вопросов и количество их использования,
                    // чтобы исключить ошибки первичного ключа БД и неверной очерёдности вывода вопросов
                    if (quizzes[startIndex].Id > 0)
                        quizzes[startIndex].Id = 0;
                    if (quizzes[startIndex].UseTimes > 0)
                        quizzes[startIndex].UseTimes = 0;
                    if (quizzes[endIndex].Id > 0)
                        quizzes[endIndex].Id = 0;
                    if (quizzes[endIndex].UseTimes > 0)
                        quizzes[endIndex].UseTimes = 0;
                }
            }

            // если требуется затереть предыдущий пак, делает это, удаляя вопросы из очереди
            if (deleteOldQuizzes)
            {
                lock (QueueLocker)
                {
                    Quizzes.Clear();
                }
                DataBaseManager.DeleteAllQuizzes();
            }

            // загружает новые вопросы в базу
            DataBaseManager.AddQuizzes(quizzes, images);

            // если все бывшие вопросы были стёрты, сразу загружает новые в очередь вопросов
            if (deleteOldQuizzes)
                LoadQuizzes();

            // удаляет временные временную папку
            if (Directory.Exists(tempDir))
                Directory.Delete(tempDir, true);

            // возвращает количество добавленных вопросов
            return quizzes.Length;
        }

        /// <summary>
        /// Удаляет все данные из базы данных и очереди вопросов
        /// </summary>
        internal void ClearAll()
        {
            lock (QueueLocker)
            {
                Quizzes.Clear();
            }
            DataBaseManager.DeleteAllQuizzes();
            DataBaseManager.DeleteAllUsers();
        }

        /// <summary>
        /// Формирует строку с топом игроков
        /// </summary>
        /// <returns>Строка с сообщением для чата</returns>
        internal static (bool result, string message) Top()
        {
            // получает из базы таблицу лидеров
            var count = int.Parse(SettingsManager.CurrentSettings[SettingsNames.PlayersTopCount]);
            var topPlayers = DataBaseManager
                .TopPlayers(count)
                .ToList();
            if (!topPlayers.Any())
                return (false, "В базе данных ещё нет игроков. Начните играть, и они появятся.");

            // формирует строку вывода
            var builder = new StringBuilder();
            var place = 1;
            var expNames = SettingsManager.CurrentSettings[SettingsNames.PlayersExpName].Split(',');
            var exp = new NumericName(expNames[0], expNames[1], expNames[2]);
            var ans = new NumericName("ответ", "ответа", "ответов");

            foreach (var player in topPlayers)
            {
                builder.Append($"\n    № {place++} - **{player.Name}**: " +
                               $"{player.Score} {exp.GetStringByNumber(player.Score)} (за {player.Answers} {ans.GetStringByNumber(player.Answers)}), " +
                               $"максимальная серия - {player.MaxWinningStreak} {ans.GetStringByNumber(player.MaxWinningStreak)}.");
            }

            // возвращает результат работы
            return (true, builder.ToString());
        }

    }
}
