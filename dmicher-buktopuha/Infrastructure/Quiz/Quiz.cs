﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using dmicher_buktopuha.Infrastructure.DataBase;
using dmicher_buktopuha.Infrastructure.Settings;

namespace dmicher_buktopuha.Infrastructure.Quiz
{
    /// <summary>
    /// Объект вопроса к викторине
    /// </summary>
    internal class Quiz
    {
        /// <summary>
        /// Все знаки, считающиеся в вопросах разделителем слов
        /// </summary>
        internal static char[] SpaceChars { get; } = { ' ', '\t', '\r', '\n', '_', '-', '.', ',', '’', '\'', ';', ':' };

        /// <summary>
        /// Массив знаков препинания, содержащийся в основном ответе
        /// </summary>
        internal char[] AnswerSpaceChars { get; }

        /// <summary>
        /// Знак, который скрывает буквы загаданного слова
        /// </summary>
        private char HideChar { get; }

        /// <summary>
        /// Номер вопроса в базе данных
        /// </summary>
        private int Id { get; }

        /// <summary>
        /// Местный общий рандомизатор для создания подсказок
        /// </summary>
        private static readonly Random HintsRandomizer = new((int)DateTime.Now.Ticks);

        /// <summary>
        /// Текущий статус вопроса
        /// </summary>
        internal QuizState State { get; set; } = QuizState.NotStarted;

        /// <summary>
        /// Есть ли в вопросе изображение
        /// </summary>
        internal bool HasImage { get; }

        /// <summary>
        /// Путь ко вложенной картинке
        /// </summary>
        internal string ImagePath { get; }

        /// <summary>
        /// Текстовая формулировка вопроса
        /// </summary>
        private readonly string _question;

        /// <summary>
        /// Основной ответ на вопрос
        /// </summary>
        internal string MainAnswer { get; }

        /// <summary>
        /// Приведён ли основной ответ в латинице
        /// </summary>
        internal bool IsMainAnswerInLatin { get; }

        /// <summary>
        /// Является ли основной ответ чисто числовым
        /// </summary>
        internal bool IsMainAnswerANumber { get; }

        /// <summary>
        /// Содержит ли основной ответ числовые значения
        /// </summary>
        internal bool MainAnswerContainsNumber { get; }

        /// <summary>
        /// Альтернативный ответ на вопрос
        /// </summary>
        internal IEnumerable<string> AlternativeAnswers { get; }

        /// <summary>
        /// Количество подсказок по вопросу
        /// </summary>
        internal byte HintsCount { get; }

        /// <summary>
        /// Содержание подсказки № 1
        /// </summary>
        private readonly string _mainAnswerHint1;

        /// <summary>
        /// Содержание подсказки № 2
        /// </summary>
        private readonly string _mainAnswerHint2;

        /// <summary>
        /// Очередь из попыток отгадать (игроков)
        /// </summary>
        internal Queue<UserGuess> Guesses { get; }

        /// <summary>
        /// Дата-время запуска вопроса
        /// </summary>
        internal DateTime StartTime { get; private set; }

        /// <summary>
        /// Дата-время последней подсказки
        /// </summary>
        internal DateTime LastHintTime { get; private set; }

        /// <summary>
        /// Возможные состояния вопроса
        /// </summary>
        internal enum QuizState : byte
        {
            /// <summary>
            /// Вопрос сформирован, но не запущен
            /// </summary>
            NotStarted,

            /// <summary>
            /// Вопрос начат
            /// </summary>
            Started,

            /// <summary>
            /// Первая подсказка показана
            /// </summary>
            Hint1,

            /// <summary>
            /// Вторая подсказка показана
            /// </summary>
            Hint2,

            /// <summary>
            /// Работа с вопросом завершена
            /// </summary>
            Stopped
        }

        /// <summary>
        /// Автор вопроса
        /// </summary>
        internal string Author { get; }

        /// <summary>
        /// Заполняет необходимые для работы с объектом поля
        /// </summary>
        /// <param name="question">Вопрос</param>
        /// <param name="answer">Ответ</param>
        /// <param name="alternativeAnswers">Альтернативные ответы на вопрос</param>
        /// <param name="author">Автор вопроса</param>
        /// <param name="id">Номер вопроса в базе данных</param>
        /// <param name="imagePath">Путь к изображению вопроса</param>
        public Quiz(string question, string answer, IEnumerable<string> alternativeAnswers,
            string author, int id, string imagePath = null)
        {
            Id = id;
            MainAnswer = answer.Trim();
            AnswerSpaceChars = MainAnswer.Where(c => SpaceChars.Any(x => x == c) && c != ' ').ToArray();
            _question = question.Trim();
            Guesses = new Queue<UserGuess>();
            AlternativeAnswers = alternativeAnswers;
            Author = author;
            if (!string.IsNullOrWhiteSpace(imagePath))
            {
                HasImage = true;

                if (File.Exists(imagePath))
                {
                    ImagePath = imagePath;
                }
                else
                {
                    State = QuizState.Stopped;
                    return;
                }
            }

            // определяет, что из себя представляет основной ответ (для простановки флагов)
            MainAnswerContainsNumber = new Regex(@"\d").IsMatch(MainAnswer);
            IsMainAnswerANumber = decimal.TryParse(MainAnswer.Replace('.', ','), out var dec);
            if (IsMainAnswerANumber)
                MainAnswer = dec.ToString(CultureInfo.InvariantCulture);
            IsMainAnswerInLatin = new Regex(@"[A-Za-z]").IsMatch(MainAnswer);

            var placeholder = SettingsManager.CurrentSettings[SettingsNames.QuizHideChar];
            HideChar = string.IsNullOrWhiteSpace(placeholder)
                ? '★'
                : placeholder[0];

            var answerLengthWithoutSpaces = answer.Length;
            foreach (var _ in answer.Where(ch => SpaceChars.Any(x => x == ch)))
                answerLengthWithoutSpaces--;

            // если ответ пустой, то вопрос пропускается - не валидные данные
            if (answerLengthWithoutSpaces < 1)
            {
                State = QuizState.Stopped;
                return;
            }

            // если ответ в одну букву, то никаких подсказок
            if (answerLengthWithoutSpaces == 1)
            {
                HintsCount = 0;
                return;
            }

            // создаёт подсказки
            var builder = new StringBuilder();

            // если ответ от 2 и до 4 букв - 1 подсказка в 1 букву
            if (answerLengthWithoutSpaces <= 4)
            {
                HintsCount = 1;
                var place = HintsRandomizer.Next(MainAnswer.Length);
                for (var i = 0; i < MainAnswer.Length; ++i)
                    builder.Append(i == place || SpaceChars.Any(x => x == MainAnswer[i])
                        ? MainAnswer[i] == ' ' ? "   " : MainAnswer[i]
                        : HideChar);
                _mainAnswerHint1 = builder.ToString();
            }
            // если ответ от 5 до 6 букв, 2 подсказки: первая 1 буква, вторая - ещё 1 буква
            else if (answerLengthWithoutSpaces <= 6)
            {
                HintsCount = 2;
                // первая подсказка: выбирает случайную букву и заменяет все буквы, кроме неё на звёздочки
                var place1 = HintsRandomizer.Next(MainAnswer.Length);
                while (SpaceChars.Any(x => x == MainAnswer[place1])) // сдвиг вправо, если попал в пробельный символ
                    if (++place1 >= MainAnswer.Length)
                        place1 = 0;
                for (var i = 0; i < MainAnswer.Length; ++i)
                    builder.Append(i == place1 || SpaceChars.Any(x => x == MainAnswer[i])
                        ? MainAnswer[i] == ' ' ? "   " : MainAnswer[i]
                        : HideChar);
                _mainAnswerHint1 = builder.ToString();
                builder.Clear();

                // вторая подсказка: берёт ещё одну случайную букву, которая не стоит на той же позиции, 
                // что и буква первой подсказки, и проводит замену, аналогичную предыдущей
                int place2;
                do
                {
                    place2 = HintsRandomizer.Next(MainAnswer.Length);
                } while (place2 == place1 || SpaceChars.Any(x => x == MainAnswer[place2]));
                for (var i = 0; i < MainAnswer.Length; ++i)
                    builder.Append(i == place2 || i == place1 || SpaceChars.Any(x => x == MainAnswer[i])
                        ? MainAnswer[i] == ' ' ? "   " : MainAnswer[i]
                        : HideChar);
                _mainAnswerHint2 = builder.ToString();
            }
            // если букв 7 и больше, 2 подсказки: каждая новая открывает треть букв ответа
            else
            {
                // две подсказки по количеству букв, равному трети от длины слова без пробелов
                HintsCount = 2;
                var charsByHint = answerLengthWithoutSpaces / 3;

                // первая подсказка: выбирает случайную треть из исходного слова
                var arr1 = new int[charsByHint];
                for (var i = 0; i < arr1.Length; ++i)
                {
                    if (i == 0)
                    {
                        arr1[i] = HintsRandomizer.Next(MainAnswer.Length);
                        continue;
                    }
                    // чтобы без повторений и пробелов
                    int placeCount;
                    int newPlace;
                    do
                    {
                        placeCount = 0;
                        newPlace = HintsRandomizer.Next(MainAnswer.Length);
                        for (var j = 0; j <= i; ++j)
                            if (arr1[j] == newPlace || SpaceChars.Any(x => x == MainAnswer[newPlace]))
                            {
                                placeCount++;
                                break;
                            }
                    } while (placeCount > 1);

                    arr1[i] = newPlace;
                }
                for (var i = 0; i < MainAnswer.Length; ++i) // заполняет  подсказку
                    builder.Append(arr1.Any(x => x == i) || SpaceChars.Any(x => x == MainAnswer[i])
                        ? MainAnswer[i] == ' ' ? "   " : MainAnswer[i]
                        : HideChar);
                _mainAnswerHint1 = builder.ToString();
                builder.Clear();

                // вторая подсказка: берёт часть из первой подсказки и дополняет ещё третью букв
                var arr2 = new int[charsByHint * 2];
                arr1.CopyTo(arr2, 0);
                for (var i = arr1.Length; i < arr2.Length; ++i)
                {
                    int placeCount;
                    int newPlace;
                    do // без повторений и пробелов
                    {
                        placeCount = 0;
                        newPlace = HintsRandomizer.Next(MainAnswer.Length);
                        for (var j = 0; j <= i; ++j)
                            if (arr2[j] == newPlace || SpaceChars.Any(x => x == MainAnswer[newPlace]))
                            {
                                placeCount++;
                                break;
                            }
                    } while (placeCount > 0);

                    arr2[i] = newPlace;
                }
                for (var i = 0; i < MainAnswer.Length; ++i) // заполняет  подсказку
                    builder.Append(arr2.Any(x => x == i) || SpaceChars.Any(x => x == MainAnswer[i])
                        ? MainAnswer[i] == ' ' ? "   " : MainAnswer[i]
                        : HideChar);
                _mainAnswerHint2 = builder.ToString();
            }
        }

        /// <summary>
        /// Запускает вопрос в работу
        /// </summary>
        /// <returns>Возвращает вопрос вызывающему коду</returns>
        internal string Start()
        {
            if (State != QuizState.NotStarted)
                return null;
            DataBaseManager.AddUseTimeOfQuiz(Id);
            State = QuizState.Started;
            LastHintTime = DateTime.Now;
            StartTime = LastHintTime;
            return _question;
        }

        /// <summary>
        /// Проверяет все ответы в очереди: если очередной ответ верный, возвращает победителя.
        /// Если верных ответов нет, возвращает 0
        /// </summary>
        /// <returns>0 или Id победителя</returns>
        internal (ulong, string, uint) CheckWinner(uint timeTiGuess)
        {
            while (Guesses.Count > 0)
            {
                static string ClearAnswer(string quizAnswer) 
                    => quizAnswer.Trim().ToLower().Replace('ё', 'е').Replace('’', '\'');
                
                var currentGuess = Guesses.Dequeue();
                var userAnswer = ClearAnswer(currentGuess.Guess);

                    // основной ответ не угадан
                if (!string.Equals(userAnswer, ClearAnswer(MainAnswer), StringComparison.InvariantCultureIgnoreCase)
                    // ни один из альтернативных ответов не угадан
                    && !(AlternativeAnswers?.Any(x => string.Equals(userAnswer, ClearAnswer(x), StringComparison.InvariantCultureIgnoreCase)) ?? false)) 
                    continue;
                State = QuizState.Stopped;

                return (
                    currentGuess.UserId,
                    currentGuess.DiscordUserName,
                    (uint)((1 - (DateTime.Now - StartTime).TotalSeconds
                    / timeTiGuess) * uint.Parse(SettingsManager.CurrentSettings[SettingsNames.QuizScoreBase])));
            }

            return (0, null, 0);
        }

        /// <summary>
        /// Флаг того, что вопрос ещё может выдавать подсказки
        /// </summary>
        internal bool IsHintsRemaining
            => HintsCount == 1 && State is QuizState.NotStarted or QuizState.Started
               || HintsCount == 2 && State is QuizState.NotStarted or QuizState.Started or QuizState.Hint1;

        /// <summary>
        /// Возвращает следующую подсказку (если она позволена этим вопросом), либо null.
        /// </summary>
        internal string NextHint
        {
            get
            {
                switch (HintsCount)
                {
                    case 1 when State == QuizState.Started:
                        State = QuizState.Hint1;
                        LastHintTime = DateTime.Now;
                        return _mainAnswerHint1;
                    case 2 when State == QuizState.Started:
                        State = QuizState.Hint1;
                        LastHintTime = DateTime.Now;
                        return _mainAnswerHint1;
                    case 2 when State == QuizState.Hint1:
                        State = QuizState.Hint2;
                        LastHintTime = DateTime.Now;
                        return _mainAnswerHint2;
                    default:
                        return null;
                }
            }
        }

    }
}
