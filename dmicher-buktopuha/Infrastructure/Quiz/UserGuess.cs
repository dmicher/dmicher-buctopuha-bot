﻿// ReSharper disable PropertyCanBeMadeInitOnly.Global
namespace dmicher_buktopuha.Infrastructure.Quiz
{
    /// <summary>
    /// Объект "ответ игрока" (предположение о том, какой ответ)
    /// </summary>
    internal class UserGuess
    {
        /// <summary>
        /// Id пользователя в Discord
        /// </summary>
        internal ulong UserId { get; set; }

        /// <summary>
        /// Имя пользователя, полученное из Discord
        /// </summary>
        internal string DiscordUserName { get; set; }

        /// <summary>
        /// Догадка игрока
        /// </summary>
        internal string Guess { get; set; }
    }
}
