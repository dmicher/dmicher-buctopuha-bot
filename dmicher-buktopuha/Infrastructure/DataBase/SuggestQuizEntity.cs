﻿using System.ComponentModel.DataAnnotations;
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace dmicher_buktopuha.Infrastructure.DataBase
{
    /// <summary>
    /// Класс предложенных вопросов
    /// </summary>
    internal class SuggestQuizEntity
    {
        /// <summary>
        /// Идентификатор предлагаемого вопроса (для БД)
        /// </summary>
        [Key]
        public int Id { get; set; }
        
        /// <summary>
        /// Вопрос
        /// </summary>
        public string TextInformation { get; set; }

        /// <summary>
        /// Все вложенные файлы в сообщение
        /// </summary>
        public byte[] AttachmentsZip { get; set; }

    }
}
