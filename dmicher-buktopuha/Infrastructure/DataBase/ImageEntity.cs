﻿using System;
using System.ComponentModel.DataAnnotations;

namespace dmicher_buktopuha.Infrastructure.DataBase
{
    /// <summary>
    /// Сущность картинки в базе данных
    /// </summary>
    public class ImageEntity
    {
        /// <summary>
        /// Идентификатор картинки
        /// </summary>
        [Key]
        public uint Id { get; set; }

        /// <summary>
        /// Имя картинки
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Массив байт самой картинки
        /// </summary>
        public byte[] Picture { get; set; }
    }
}
