﻿using System;
using System.ComponentModel.DataAnnotations;
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable AutoPropertyCanBeMadeGetOnly.Global

namespace dmicher_buktopuha.Infrastructure.DataBase
{
    /// <summary>
    /// Сущность "вопрос" в базе данных
    /// </summary>
    public class QuizEntity
    {
        /// <summary>
        /// Первичный ключ для базы данных
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Вопрос
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// Ответ
        /// </summary>
        public string MainAnswer { get; set; }

        /// <summary>
        /// Альтернативные ответы на вопрос
        /// </summary>
        public string AlternativeAnswers { get; set; }
        
        /// <summary>
        /// последнее время, когда вопрос доставался из базы данных
        /// </summary>
        public uint UseTimes { get; set; }
        
        /// <summary>
        /// Автор вопроса
        /// </summary>
        public string Author { get; set; }

        /// <summary>
        /// Изображение, сопутствующее вопросу
        /// </summary>
        public Guid? ImageGuid { get; set; } = null;

        /// <summary>
        /// Флаг того, что вопрос имеет изображение
        /// </summary>
        public bool HasImage => ImageGuid != null;
    }
}
