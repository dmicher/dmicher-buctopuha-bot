﻿using System.ComponentModel.DataAnnotations;
// ReSharper disable PropertyCanBeMadeInitOnly.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable ClassNeverInstantiated.Global

namespace dmicher_buktopuha.Infrastructure.DataBase
{
    /// <summary>
    /// Сущность "игрок" в базе данных
    /// </summary>
    public class PlayerEntity
    {
        /// <summary>
        /// Первичный ключ для базы данных
        /// </summary>
        [Key]
        public ulong Id { get; set; }

        /// <summary>
        /// Идентификатор пользователя в Discord
        /// </summary>
        public ulong DiscordId { get; set; }

        /// <summary>
        /// Отображаемое имя игрока
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Счёт игрока
        /// </summary>
        public uint Score { get; set; }

        /// <summary>
        /// Количество верных ответов игрока
        /// </summary>
        public uint Answers { get; set; }

        /// <summary>
        /// Максимальная длина победных серий
        /// </summary>
        public uint MaxWinningStreak { get; set; }
    }
}
