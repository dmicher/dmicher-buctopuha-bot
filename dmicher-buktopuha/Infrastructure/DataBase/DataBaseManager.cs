﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace dmicher_buktopuha.Infrastructure.DataBase
{
    /// <summary>
    /// Объект, управляющий базой данных вопросов
    /// </summary>
    internal static class DataBaseManager
    {
        /// <summary>
        /// Путь к папке с временными изображениями для вопросов
        /// </summary>
        private static readonly string ImageDirPath = Path.Combine(AppContext.BaseDirectory, "App_Data", "tmpImg");

        /// <summary>
        /// очищает папку с временными файлами изображений
        /// </summary>
        internal static void PrepareCleanImageDirPath(string[] exceptions = null)
        {
            var dir = new DirectoryInfo(ImageDirPath);

            // Если папки нет, то она уже чиста - нужно только создать
            if (!dir.Exists)
            {
                dir.Create();
                return;
            }

            // удаляет все файлы, исключая те, которые удалять не нужно
            var allFilesInDir = dir.GetFiles();
            var filesToDelete = allFilesInDir.Where(x => exceptions?.All(y => !y.Contains(x.Name)) ?? true).ToArray();
            foreach (var imgToDelete in filesToDelete)
                imgToDelete.Delete();
        }

        /// <summary>
        /// Получает указанное количество вопросов из базы данных случайным образом
        /// </summary>
        /// <param name="count">Количество вопросов</param>
        /// <param name="percentOfOld">Количество (в процентах) загружаемых в выборку элементов по их старости</param>
        /// <returns>Вопросы, которые дольше всех ранее не были вытянуты</returns>
        internal static IEnumerable<Quiz.Quiz> GetRandomQuizzes(int count, byte percentOfOld)
        {
            using var db = new BuctopuhaContext();

            // для начала отбирает Id вопросов среди указанной части всех вопросов в базе данных,
            // отсекая (если есть возможность) ту часть вопросов, которые были использованы меньшее количество раз
            if (!db.Quizzes.Any())
                return null;

            var minId = db.Quizzes.Min(x => x.Id);
            var maxId = db.Quizzes.Max(x => x.Id);
            var fullRange = maxId + 1 - minId;
            var selectRange = fullRange <= count
                ? fullRange
                : fullRange * (percentOfOld > 100 ? 100 : percentOfOld) / 100;

            var leastOftenQuizzes = ((IEnumerable<QuizEntity>)db.Quizzes)
                .OrderBy(x => x.UseTimes)
                .Select(x => (x.Id, x.MainAnswer))
                .Take(selectRange)
                .ToArray();

            // из полученной выборки случайно выбирает Id вопросов и значения основного вопроса, которые будут добавлены в очередь
            var idArr = new (int id, string answer)[count];
            var rand = new Random(Environment.TickCount);
            for (var i = 0; i < count; ++i)
            {
                (int id, string answer) randomQuizCharacteristicPair;
                var iteration = 0;
                do // если вытянет вопрос с повторным Id или основным ответом, несколько раз попытается перевыбрать вопрос
                {
                    randomQuizCharacteristicPair = leastOftenQuizzes[rand.Next(leastOftenQuizzes.Length)];
                } while (idArr.Any(x => x.id == randomQuizCharacteristicPair.id)
                         && idArr.Any(x => x.answer == randomQuizCharacteristicPair.answer)
                         && iteration++ < count);

                idArr[i] = randomQuizCharacteristicPair;
            }

            // вытягивает из базы данных сами вопросы (по отобранным Id), конвертирует вопрос базы данных в вопрос бота и возвращает результат
            var entities = idArr.Select(x => db.Quizzes.FirstOrDefault(y => x.id == y.Id)).ToArray();
            return ConvertEntityToQuiz(entities, db);
        }

        /// <summary>
        /// Возвращает указанное количество вопросов, идущих последовательно в базе данных (не случайная выборка)
        /// </summary>
        /// <param name="count">Количество загружаемых вопросов</param>
        /// <returns>Коллекция вопросов</returns>
        internal static IEnumerable<Quiz.Quiz> GetNextQuizzes(int count)
        {
            using var db = new BuctopuhaContext();
            if (!db.Quizzes.Any())
                return null;

            var minUseTime = db.Quizzes.Min(x => x.UseTimes);
            var nextUseTime = minUseTime + 1;
            var countOfMinUseTimeQuizzes = db.Quizzes.Count(x => x.UseTimes == minUseTime);
            var countOfNextUseTimeQuizzes = count - countOfMinUseTimeQuizzes;
            if (countOfNextUseTimeQuizzes < 0)
                countOfNextUseTimeQuizzes = 0;

            var entities = (db.Quizzes as IEnumerable<QuizEntity>)
                .Where(x => x.UseTimes == minUseTime)
                .Take(countOfMinUseTimeQuizzes)
                .Concat((db.Quizzes as IEnumerable<QuizEntity>)
                    .Where(x => x.UseTimes == nextUseTime)
                    .Take(countOfNextUseTimeQuizzes));

            return ConvertEntityToQuiz(entities, db);
        }

        /// <summary>
        /// Преобразует коллекцию сущностей вопросов в базе данных в коллекцию вопросов для бота
        /// </summary>
        /// <param name="entities">Сущности базы данных</param>
        /// <param name="db">Открытый контекст базы данных</param>
        /// <returns>Коллекция вопросов для бота</returns>
        private static IEnumerable<Quiz.Quiz> ConvertEntityToQuiz(IEnumerable<QuizEntity> entities, BuctopuhaContext db)
        {
            var quizList = new List<Quiz.Quiz>();
            foreach (var entity in entities)
            {
                string imagePath = null;
                // подгружает картинку, если она есть в вопросе и может быть загружена
                if (entity.ImageGuid != null)
                {
                    var image = db.Images.FirstOrDefault(x => x.Name.Contains(entity.ImageGuid.ToString()));
                    if (image != null)
                    {
                        if (!Directory.Exists(ImageDirPath))
                            Directory.CreateDirectory(ImageDirPath);
                        imagePath = Path.Combine(ImageDirPath, image.Name);
                        File.WriteAllBytes(imagePath, image.Picture);
                    }
                }

                var quiz = new Quiz.Quiz(entity.Question, entity.MainAnswer,
                    entity.AlternativeAnswers?.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries), entity.Author,
                    entity.Id, imagePath);
                quizList.Add(quiz);
            }

            return quizList;
        }

        /// <summary>
        /// Перемешивает последовательность вопросов в базе данных
        /// </summary>
        internal static void ShuffleAllQuizzesInDataBase()
        {
            using var db = new BuctopuhaContext();

            var currentQuizzes= db.Quizzes;
            var quizzes = new QuizEntity[currentQuizzes.Count()];
            currentQuizzes.ToArray().CopyTo(quizzes, 0);
            db.Quizzes.RemoveRange(currentQuizzes);
            db.SaveChanges();

            var rand = new Random(Environment.TickCount);
            for (var runNumber = 0; runNumber < 10; runNumber++)
            {
                // несколько раз перемешивает массив, чтобы добиться более-менее случайного распределения вопросов
                // (тематически похожие вопросы могут быть написаны одним автором друг рядом с другом - этого следует избегать)
                for (var startIndex = quizzes.Length - 1; startIndex >= 1; startIndex--)
                {
                    var endIndex = rand.Next(startIndex + 1);
                    (quizzes[endIndex], quizzes[startIndex]) = (quizzes[startIndex], quizzes[endIndex]);
                    if (runNumber > 0) continue;

                    // при первом перемешивании обнуляет номера вопросов и количество их использования,
                    // чтобы исключить ошибки первичного ключа БД и неверной очерёдности вывода вопросов
                    if (quizzes[startIndex].Id > 0)
                        quizzes[startIndex].Id = 0;
                    if (quizzes[startIndex].UseTimes > 0)
                        quizzes[startIndex].UseTimes = 0;
                    if (quizzes[endIndex].Id > 0)
                        quizzes[endIndex].Id = 0;
                    if (quizzes[endIndex].UseTimes > 0)
                        quizzes[endIndex].UseTimes = 0;
                }
            }

            db.Quizzes.AddRange(quizzes);
            db.SaveChanges();
        }

        /// <summary>
        /// Учитывает, что указанный вопрос был использован (выведен в чат игрокам) ботом
        /// </summary>
        /// <param name="id">Номер вопроса</param>
        internal static void AddUseTimeOfQuiz(int id)
        {
            using var db = new BuctopuhaContext();
            var quiz = db.Quizzes.FirstOrDefault(x => x.Id == id);
            if (quiz == null)
                return;
            quiz.UseTimes++;
            db.SaveChanges();
        }

        /// <summary>
        /// Добавляет вопросы в базу данных
        /// </summary>
        /// <param name="quizEntities">Вопросы для добавления</param>
        /// <param name="images">Изображения для добавляемых вопросов</param>
        internal static void AddQuizzes(IEnumerable<QuizEntity> quizEntities, IEnumerable<ImageEntity> images)
        {
            using var db = new BuctopuhaContext();
            db.Quizzes.AddRange(quizEntities);
            var imageArr = images.ToArray();
            if (imageArr.Length > 0)
                db.Images.AddRange(imageArr);
            db.SaveChanges(true);
        }

        /// <summary>
        /// Удаляет все вопросы из базы данных
        /// </summary>
        internal static void DeleteAllQuizzes()
        {
            using var db = new BuctopuhaContext();
            db.Quizzes.RemoveRange(db.Quizzes);
            db.Images.RemoveRange(db.Images);
            db.SaveChanges(true);
        }

        /// <summary>
        /// Удаляет всех пользователей из базы данных
        /// </summary>
        internal static void DeleteAllUsers()
        {
            using var db = new BuctopuhaContext();
            db.Players.RemoveRange(db.Players);
            db.SaveChanges();
        }

        /// <summary>
        /// Добавляет указанному пользователю указанное количество очков и один факт
        /// ответа на вопрос. Если пользователя нет, добавляет.
        /// </summary>
        /// <param name="discordId">Пользователь Discord (его идентификатор)</param>
        /// <param name="score">Добавляемый счёт</param>
        /// <param name="streak">Серия ответов</param>
        /// <param name="discordName">Имя игрока, полученное из Discord</param>
        internal static string AddScoreAndGetName(ulong discordId, uint score, uint streak, string discordName)
        {
            using var db = new BuctopuhaContext();
            var player = db.Players.FirstOrDefault(x => x.DiscordId == discordId);
            if (player != null)
            {
                player.Score += score;
                player.Answers++;
                player.MaxWinningStreak = Math.Max(player.MaxWinningStreak, streak);
                if (string.IsNullOrWhiteSpace(player.Name))
                    player.Name = discordName;
                db.SaveChanges();
                return player.Name;
            }

            player = new PlayerEntity
            {
                DiscordId = discordId,
                Answers = 1,
                Score = score,
                MaxWinningStreak = streak,
                Name = discordName
            };
            db.Players.Add(player);
            db.SaveChanges();
            return player.Name;
        }

        /// <summary>
        /// Возвращает количество игроков, чей счёт (меньше счёта, равен счёту) 
        /// игрока с указанным идентификатором Discord
        /// </summary>
        /// <param name="discordId">идентификатор в Discord</param>
        /// <returns>1. количество игроков, имеющий меньший счёт, 2. Количество игроков, имеющий тот же счёт</returns>
        internal static (long position, long samescore) GetPosition(ulong discordId)
        {
            using var db = new BuctopuhaContext();
            var player = db.Players.FirstOrDefault(x => x.DiscordId == discordId);
            return player is null
                ? (db.Players.Count() + 1, 0)
                : (
                    (db.Players.Count() - db.Players.Count(x => x.Score < player.Score)),
                    (db.Players.Count(x => x.Score == player.Score) - 1)
                );
        }

        /// <summary>
        /// Обновляет отображаемое имя пользователя в базе данных
        /// </summary>
        /// <param name="discordId">Идентификатор пользователя в Discord</param>
        /// <param name="newName">Новое имя пользователя</param>
        internal static (bool, string) UpdatePlayerName(ulong discordId, string newName)
        {
            using var db = new BuctopuhaContext();
            var existingPlayer = db.Players.FirstOrDefault(x => x.Name == newName);
            if (existingPlayer is not null)
                return (false, discordId == existingPlayer.DiscordId
                    ? $"Выбранное имя **{newName}** и так уже занято вами."
                    : $"Выбранное имя **{newName}** уже занято другим пользователем.");

            var player = db.Players.FirstOrDefault(x => x.DiscordId == discordId);
            if (player is null)
            {
                db.Players.Add(new PlayerEntity
                {
                    DiscordId = discordId,
                    Name = newName,
                    Answers = 0,
                    MaxWinningStreak = 0,
                    Score = 0
                });
            }
            else
            {
                player.Name = newName;
            }
            db.SaveChanges();
            return (true, null);
        }

        /// <summary>
        /// Возвращает из базы данных топ <paramref name="count"/> игроков
        /// </summary>
        /// <param name="count">Количество выгружаемых лидеров</param>
        /// <returns>Перечень лидеров</returns>
        internal static IEnumerable<PlayerEntity> TopPlayers(int count)
        {
            using var db = new BuctopuhaContext();
            return ((IEnumerable<PlayerEntity>)db.Players)
                .OrderByDescending(x => x.Score)
                .Take(count).ToArray();
        }

        /// <summary>
        /// Возвращает кортеж из трёх значений: количество ответов, накопленный счёт и позиция в топе
        /// </summary>
        /// <param name="discordId">Идентификатор игрока в Discord</param>
        /// <returns>1. Количество ответов игрока, 2. Счёт игрока</returns>
        internal static (PlayerEntity, int) ScoreByPlayer(ulong discordId)
        {
            using var db = new BuctopuhaContext();
            if (!db.Players.Any())
                return (null, -1);
            var player = db.Players.FirstOrDefault(x => x.DiscordId == discordId);
            return (player, db.Players.Count(x => x.Score > player.Score) + 1);
        }

        /// <summary>
        /// Добавляет очередной предлагаемый игроком вопрос в базу данных
        /// </summary>
        /// <param name="text">Текстовая информация предлагаемого вопроса</param>
        /// <param name="attachmentsInZip">все вложения сообщения с предложением</param>
        /// <returns>Результат добавления вопроса</returns>
        internal static void SuggestNewQuiz(string text, byte[] attachmentsInZip = null)
        {
            using var db = new BuctopuhaContext();
            db.Suggestions.Add(new SuggestQuizEntity
            {
                TextInformation = text,
                AttachmentsZip = attachmentsInZip
            });
            db.SaveChanges();
        }

        /// <summary>
        /// Возвращает количество предложенных вопросов, хранимых в базе данных
        /// </summary>
        /// <returns>Количество зарегистрированных предложений вопросов</returns>
        internal static int SuggestionsCount()
        {
            using var db = new BuctopuhaContext();
            return db.Suggestions?.Count() ?? 0;
        }

        /// <summary>
        /// Возвращает Id указанного количества вопросов
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        internal static int[] GetSuggestionsIds(int count)
        {
            using var db = new BuctopuhaContext();
            return ((IEnumerable<SuggestQuizEntity>)db.Suggestions)
                .OrderBy(x => x.Id)
                .Select(x => x.Id)
                .Take(count)
                .ToArray();
        }

        /// <summary>
        /// Возвращает сущность предложенного вопроса по его номеру
        /// </summary>
        /// <param name="id">номер предложения</param>
        /// <returns>Сущность вопроса</returns>
        internal static SuggestQuizEntity GetSuggestion(int id)
        {
            using var db = new BuctopuhaContext();
            return db.Suggestions.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Удаляет из базы данных предложение по его идентификатору
        /// </summary>
        /// <param name="id"></param>
        internal static void DeleteSuggestion(int id)
        {
            using var db = new BuctopuhaContext();
            var entity = db.Suggestions.FirstOrDefault(x => x.Id == id);
            if (entity is null) return;
            db.Suggestions.Remove(entity);
            db.SaveChanges();
        }

    }
}
