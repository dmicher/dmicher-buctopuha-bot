﻿using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace dmicher_buktopuha.Infrastructure.DataBase
{
    /// <summary>
    /// Контекст базы данных
    /// </summary>
    internal sealed class BuctopuhaContext : DbContext
    {
        /// <summary>
        /// Набор данных "вопросы викторины"
        /// </summary>
        internal DbSet<QuizEntity> Quizzes { get; set; }

        /// <summary>
        /// Набор данных "Игроки"
        /// </summary>
        internal DbSet<PlayerEntity> Players { get; set; }

        /// <summary>
        /// Набор картинок для вопросов
        /// </summary>
        internal DbSet<ImageEntity> Images { get; set; }

        /// <summary>
        /// Вопросы, предлагаемые игроками
        /// </summary>
        internal DbSet<SuggestQuizEntity> Suggestions { get; set; }

        /// <summary>
        /// Создаёт базу данных, если она не создана
        /// </summary>
        public BuctopuhaContext() => Database.EnsureCreated();

        /// <summary>
        /// Определяет расположение базы данных (она встроенная, поэтому строка подключения в коде)
        /// </summary>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseSqlite($"Data Source={Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "App_Data", "quizzes.db")}");
    }
}
