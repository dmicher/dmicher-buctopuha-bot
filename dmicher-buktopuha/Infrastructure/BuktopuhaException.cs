﻿using System;

namespace dmicher_buktopuha.Infrastructure
{
    /// <summary>
    /// Внутренние ошибки приложения
    /// </summary>
    public class BuktopuhaException : Exception
    {
        /// <summary>
        /// Инициирует объект с сообщением
        /// </summary>
        /// <param name="message">сообщение</param>
        public BuktopuhaException(string message) : base(message)
        {
        }
    }
}
