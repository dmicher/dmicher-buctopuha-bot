﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace dmicher_buktopuha.Infrastructure.Settings
{
    /// <summary>
    /// Объект, читающий настройки
    /// </summary>
    internal static class SettingsManager
    {
        /// <summary>
        /// Текущие загруженные настройки приложения
        /// </summary>
        internal static Dictionary<SettingsNames, string> CurrentSettings { get; private set; }

        /// <summary>
        /// Путь к файлу, хранящему настройки приложения
        /// </summary>
        private static string SettingsFilePath =>
            Path.Combine(AppContext.BaseDirectory, "App_Data", "app_settings.ini");

        /// <summary>
        /// Читает (перечитывает) настройки приложения
        /// </summary>
        internal static void ReadSettings() =>
            CurrentSettings = File.ReadAllLines(SettingsFilePath)
                .Select(x => x.Split('='))
                .Where(x => x.Length == 2)
                .ToDictionary(x => Enum.Parse<SettingsNames>(x[0].Trim()), x => x[1].Trim());

        /// <summary>
        /// Изменяет текущие настройки приложения и сохраняет в файл
        /// </summary>
        /// <param name="newSettings">Словарь с новыми настройками (может не дублировать словарь со всеми настройками)</param>
        internal static void ChangeAndSaveSetting(Dictionary<SettingsNames, string> newSettings)
        {
            foreach (var (key, value) in newSettings)
            {
                if (CurrentSettings.ContainsKey(key))
                    CurrentSettings[key] = value;
                else
                    CurrentSettings.Add(key, value);
            }

            File.WriteAllLines(SettingsFilePath, CurrentSettings.Select(x => x.Key + "=" + x.Value));
        }

    }
}
