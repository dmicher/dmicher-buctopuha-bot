﻿namespace dmicher_buktopuha.Infrastructure.Settings
{
    /// <summary>
    /// Перечисление возможных настроек в ini-файле
    /// </summary>
    public enum SettingsNames
    {
        /// <summary>
        /// Токен бота в дискорд
        /// </summary>
        DiscordToken,

        /// <summary>
        /// Id администратора бота в дискорд (повышенные привилегии)
        /// </summary>
        DiscordAdministratorsIds,

        /// <summary>
        /// Id гильдии (сервера) Discord  для работы с вопросами и ответами
        /// </summary>
        DiscordQuizzesGuildId,

        /// <summary>
        /// Id ветки для работы с вопросами и ответами
        /// </summary>
        DiscordQuizzesChannelId,
        
        /// <summary>
        /// Id ветки для управления ботом
        /// </summary>
        DiscordManageChannelId,
        
        /// <summary>
        /// Id бота в Discord
        /// </summary>
        DiscordBotId,

        /// <summary>
        /// Время в секундах, через которое будет показана подсказка (если она есть)
        /// </summary>
        QuizHintTimeSpan,

        /// <summary>
        /// Время в секундах, отводимое на весь вопрос в целом (должно быть не меньше
        /// времени подсказки, умноженного на три
        /// </summary>
        QuizQuestionTimeSpan,
        
        /// <summary>
        /// Время отдыха между вопросами
        /// </summary>
        QuizSleepBetweenQuestions,

        /// <summary>
        /// Критически низкое количество оставшихся вопросов в очереди,
        /// когда пора загружать новые вопросы из базы данных
        /// </summary>
        QuizzesLoadWhenRemain,

        /// <summary>
        /// Сколько вопросов из базы данных загружать за раз
        /// </summary>
        QuizzesLoadCount,

        /// <summary>
        /// Количество (в процентах) вопросов, которые могут попасть в следующую выборку
        /// вопросов, исключая те вопросы, которые были заданы игрокам недавно
        /// </summary>
        QuizzesLoadPercentOfOld,

        /// <summary>
        /// Число, определяющее базовый компонент для расчёта очков, получаемых
        /// игроком за правильный ответ: чем раньше, тем больше
        /// </summary>
        QuizScoreBase,

        /// <summary>
        /// Символ, скрывающий закрытые буквы слова
        /// </summary>
        QuizHideChar,

        /// <summary>
        /// Приветствие, которое будет выдавать бот, когда 
        /// </summary>
        QuizGreetings,

        /// <summary>
        /// Показывать ли ответ, если никто не угадал
        /// </summary>
        QuizShowAnswerWhenLoose,

        /// <summary>
        /// Показывать ли альтернативные ответы вместе с основным
        /// </summary>
        QuizShowAlternativeAnswers,

        /// <summary>
        /// Показывать ли авторов вопросов
        /// </summary>
        QuizShowAuthors,

        /// <summary>
        /// название очков опыта для игроков
        /// </summary>
        PlayersExpName,

        /// <summary>
        /// Количество игроков, отображаемых в топе
        /// </summary>
        PlayersTopCount,

        /// <summary>
        /// Длина победной серии, на которой действует модификатор первого уровня,
        /// увеличивающий получаемый опыт 
        /// </summary>
        PlayersWinnerStreakLevel1,

        /// <summary>
        /// Модификатор опыта первого уровня
        /// </summary>
        PlayersWinnerStreakScoreFactor1,

        /// <summary>
        /// Длина победной серии, на которой действует модификатор второго уровня,
        /// увеличивающий получаемый опыт 
        /// </summary>
        PlayersWinnerStreakLevel2,

        /// <summary>
        /// Модификатор опыта второго уровня
        /// </summary>
        PlayersWinnerStreakScoreFactor2,

        /// <summary>
        /// Длина победной серии, на которой действует модификатор третьего уровня,
        /// увеличивающий получаемый опыт 
        /// </summary>
        PlayersWinnerStreakLevel3,

        /// <summary>
        /// Модификатор опыта третьего уровня
        /// </summary>
        PlayersWinnerStreakScoreFactor3,

        /// <summary>
        /// Период вывода таблицы лидеров после вопросов
        /// </summary>
        LeadersTableRepeatPeriod,
        
        /// <summary>
        /// Авторизация для запуска или остановки игры:
        /// 0 - любой игрок,
        /// 1 - только администратор из канала для управления
        /// </summary>
        GameStartAuthorization,

        /// <summary>
        /// Режим остановки игры:
        /// 0 - по общему количеству вопросов,
        /// 1 - по количеству вопросов, на которые не было попыток ответить,
        /// 2 - только ручное отключение.
        /// </summary>
        GameStopMode,

        /// <summary>
        /// Количество вопросов (в зависимости от <see cref="GameStopMode"/>):
        /// 0 - всего вопросов в серии;
        /// 1 - вопросов без ответов;
        /// 2 - не используется.
        /// </summary>
        GameStopQuizzesCount,

        /// <summary>
        /// Режим добавления вопросов:
        /// 0 - случайная выборка с приоритетом тех вопросов, которые давно не использовались;
        /// 1 - в порядке, указанном в базе данных, начиная с тех, которые использовались меньше всего.
        /// </summary>
        GameAddQuizzesMode
    }
}