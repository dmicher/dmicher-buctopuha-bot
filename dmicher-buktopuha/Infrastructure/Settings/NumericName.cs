﻿namespace dmicher_buktopuha.Infrastructure.Settings
{
    /// <summary>
    /// Класс для слова, которое обозначает опыт
    /// </summary>
    internal class NumericName
    {
        /// <summary>
        /// Для числе в 1: "один чемодан", "на одну позицию".
        /// </summary>
        private readonly string _for1;

        /// <summary>
        /// Для чисел от 2 до 4: "четыре чемодана", на "три позиции"
        /// </summary>
        private readonly string _forLessThan5;

        /// <summary>
        /// Для остальных чисел: "пять чемоданов", на "пять позиций"
        /// </summary>
        private readonly string _forMany;

        /// <summary>
        /// Заполняет все склонения из набора строк
        /// </summary>
        /// <param name="for1">Форма слова для получения: 1 чемодана</param>
        /// <param name="forLessThan5">Форма слова для получения: 2 и до 4 чемодана</param>
        /// <param name="forMany">Форма слова для получения: 5 чемоданов и более</param>
        public NumericName(string for1, string forLessThan5, string forMany)
        {
            _for1 = for1;
            _forLessThan5 = forLessThan5;
            _forMany = forMany;
        }

        /// <summary>
        /// Получает слово в нужной форме при указанном числе, с которым оно должно сочетаться
        /// </summary>
        /// <param name="num">Число, с которым должно сочетаться слово.</param>
        /// <returns>Слово в нужной форме</returns>
        internal string GetStringByNumber(long num)
        {
            if (num < 0)
                num = -num;

            if ((num % 100) < 21)
            {
                return num switch
                {
                    0 => _forMany,
                    1 => _for1,
                    <= 4 => _forLessThan5,
                    _ => _forMany
                };
            }

            return (num % 10) switch
            {
                0 => _forMany,
                1 => _for1,
                <= 4 => _forLessThan5,
                _ => _forMany
            };
        }
    }
}
