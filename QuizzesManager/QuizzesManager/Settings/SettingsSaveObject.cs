﻿using System;

namespace QuizzesManager.Settings
{
    [Serializable]
    public class SettingsSaveObject : IEquatable<SettingsSaveObject>
    {
        /// <summary>
        /// Файл для загрузки
        /// </summary>
        public string FromFile { get; set; }

        /// <summary>
        /// Файл для сохранения
        /// </summary>
        public string ToFile { get; set; }

        /// <summary>
        /// Использовать формат викторины
        /// </summary>
        public bool BuktopuhaFormat { get; set; }

        /// <summary>
        /// При загрузке добавлять новые вопросы (не стирать старые)
        /// </summary>
        public bool AppendNew { get; set; }

        /// <summary>
        /// Используемый разделитель при добавлении вопросов не по формату викторины
        /// </summary>
        public string Separator { get; set; }

        /// <summary>
        /// Имя автора по умолчанию
        /// </summary>
        public string DefaultAuthor { get; set; }

        /// <summary>
        /// Добавлять автора по умолчанию ко всем незаполненным вопросам
        /// </summary>
        public bool AddDefaultAuthor { get; set; }

        /// <summary>
        /// После сохранения сразу перезагрузить из файла
        /// </summary>
        public bool SaveAndLoadMode { get; set; }

        /// <summary>
        /// Уменьшать ли размер изображений до указанного количества пикселей
        /// </summary>
        public bool ResizeImages { get; set; }

        /// <summary>
        /// Размер, до которого будет изменёно изображение, если оно больше указанного
        /// </summary>
        public int ResizeImageTo { get; set; }

        /// <summary>
        /// Предупреждать ли при превышении лимита в размере файла
        /// </summary>
        public bool QuizzesFileSizeWarning { get; set; }

        /// <summary>
        /// Размер файла в мегабайтах, после которого следует предупредить пользователя
        /// </summary>
        public int QuizzesFileSizeLimit { get; set; }

        /// <summary>
        /// Сравнивает объекты по значению
        /// </summary>
        /// <param name="other">второй объект</param>
        /// <returns>успех, если все значения эквивалентны</returns>
        public bool Equals(SettingsSaveObject other)
            => other != null
               && FromFile.Equals(other.FromFile)
               && ToFile.Equals(other.ToFile)
               && BuktopuhaFormat.Equals(other.BuktopuhaFormat)
               && AppendNew.Equals(other.AppendNew)
               && Separator.Equals(other.Separator)
               && DefaultAuthor.Equals(other.DefaultAuthor)
               && AddDefaultAuthor.Equals(other.AddDefaultAuthor)
               && SaveAndLoadMode.Equals(other.SaveAndLoadMode)
               && ResizeImages.Equals(other.ResizeImages)
               && ResizeImageTo.Equals(other.ResizeImageTo)
               && QuizzesFileSizeWarning.Equals(other.QuizzesFileSizeWarning)
               && QuizzesFileSizeLimit.Equals(other.QuizzesFileSizeLimit);
    }
}
