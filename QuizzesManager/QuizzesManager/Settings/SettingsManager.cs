﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace QuizzesManager.Settings
{
    /// <summary>
    /// Сохраняет и загружает настройки программы
    /// </summary>
    internal static class SettingsManager
    {
        /// <summary>
        /// Путь к файлу настроек
        /// </summary>
        private static readonly string FilePath = Path.Combine(AppContext.BaseDirectory, "settings.xml");

        /// <summary>
        /// Сериализатор для настроек
        /// </summary>
        private static readonly XmlSerializer Serializer = new(typeof(SettingsSaveObject));

        /// <summary>
        /// Настройки по умолчанию
        /// </summary>
        private static readonly SettingsSaveObject DefaultSettings = new()
        {
            FromFile = "main.btp",
            ToFile = "main.btp",
            BuktopuhaFormat = true,
            AppendNew = false,
            Separator = "*",
            DefaultAuthor = Environment.UserName,
            AddDefaultAuthor = true,
            SaveAndLoadMode = true,
            ResizeImages = true,
            ResizeImageTo = 300,
            QuizzesFileSizeWarning = true,
            QuizzesFileSizeLimit = 7
        };

        /// <summary>
        /// Читает настройки из файла
        /// </summary>
        /// <returns></returns>
        internal static SettingsSaveObject GetSettings()
        {
            if (File.Exists(FilePath))
            {
                using var stream = File.OpenRead(FilePath);
                return (SettingsSaveObject)Serializer.Deserialize(stream);
            }

            return DefaultSettings;
        }

        /// <summary>
        /// Сохраняет настройки в файл
        /// </summary>
        /// <param name="settings"></param>
        internal static void SaveSettings(SettingsSaveObject settings)
        {
            using var stream = File.CreateText(FilePath);
            Serializer.Serialize(stream, settings);
        }
    }
}
