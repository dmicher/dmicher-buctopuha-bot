﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;
using dmicher_buktopuha.Infrastructure.DataBase;
using QuizzesManager.Settings;

namespace QuizzesManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    // ReSharper disable once RedundantExtendsListEntry
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Текущий набор объектов
        /// </summary>
        private List<QuizEntity> Quizzes { get; set; } = GetNew();

        /// <summary>
        /// Выбранный вопрос для вставки картинки (и других изменений над ним)
        /// </summary>
        public QuizEntity SelectedEntity { get; private set; }

        /// <summary>
        /// Возвращает новый "пустой" список
        /// </summary>
        private static List<QuizEntity> GetNew() => new() { new QuizEntity { Question = "Напишите первый вопрос", MainAnswer = "Ответ" } };

        /// <summary>
        /// Сериализатор
        /// </summary>
        private readonly XmlSerializer _serializer = new(typeof(QuizEntity[]));

        /// <summary>
        /// путь к файлу пака
        /// </summary>
        private string DataDirectory { get; } = Path.Combine(AppContext.BaseDirectory, "Data");

        /// <summary>
        /// Настройки программы
        /// </summary>
        private SettingsSaveObject _settings;

        /// <summary>
        /// Запуск окна
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            LoadSettings();
        }

        /// <summary>
        /// Загружает настройки в файл
        /// </summary>
        private void LoadSettings()
        {
            _settings = SettingsManager.GetSettings();

            FromFile.Text = _settings.FromFile;
            ToFile.Text = _settings.ToFile;
            Separator.Text = _settings.Separator;
            BuktopuhaFormat.IsChecked = _settings.BuktopuhaFormat;
            SaveAndLoadMode.IsChecked = _settings.SaveAndLoadMode;
            DefaultAuthor.Text = _settings.DefaultAuthor;
            AddDefaultAuthor.IsChecked = _settings.AddDefaultAuthor;
            Resize.IsChecked = _settings.ResizeImages;
            ResizeTo.Text = _settings.ResizeImageTo.ToString();
            QuizzesFileSizeWarning.IsChecked = _settings.QuizzesFileSizeWarning;
            QuizzesFileSizeWarningLimit.Text = _settings.QuizzesFileSizeLimit.ToString();
        }

        /// <summary>
        /// Сохраняет настройки в файл
        /// </summary>
        private void SaveSettings()
        {
            _settings.FromFile = FromFile.Text;
            _settings.ToFile = ToFile.Text;
            _settings.Separator = Separator.Text;
            _settings.BuktopuhaFormat = BuktopuhaFormat?.IsChecked ?? true;
            _settings.SaveAndLoadMode = SaveAndLoadMode?.IsChecked ?? true;
            _settings.DefaultAuthor = DefaultAuthor.Text;
            _settings.AddDefaultAuthor = AddDefaultAuthor?.IsChecked ?? true;
            _settings.ResizeImages = Resize.IsChecked ?? true;
            _settings.ResizeImageTo = int.Parse(ResizeTo.Text);
            _settings.QuizzesFileSizeWarning = QuizzesFileSizeWarning.IsChecked ?? false;
            _settings.QuizzesFileSizeLimit = int.Parse(QuizzesFileSizeWarningLimit.Text);

            SettingsManager.SaveSettings(_settings);
        }

        /// <summary>
        /// Загружает данные из файла
        /// </summary>
        private void LoadQuizzes()
        {
            var path = Path.Combine(DataDirectory, FromFile.Text);
            var dir = Path.Combine(DataDirectory, "work");
            if (Directory.Exists(dir))
                Directory.Delete(dir, true);

            Directory.CreateDirectory(dir);

            foreach (var fileName in Directory.GetFiles(dir))
                File.Delete(fileName);

            if (File.Exists(path))
            {
                if (FromFile.Text.EndsWith(".btp"))
                {
                    ZipFile.ExtractToDirectory(path, dir);
                }
                else
                {
                    File.Move(path, Path.Combine(dir, "quizzes.xml"));
                }

                path = Path.Combine(dir, "quizzes.xml");

                IEnumerable<QuizEntity> entities = null;
                if (File.Exists(path))
                {
                    if (BuktopuhaFormat.IsChecked ?? true)
                    {
                        using var stream = File.OpenRead(path);
                        entities = (QuizEntity[])_serializer.Deserialize(stream);
                    }
                    else
                    {
                        entities = File.ReadAllLines(path)
                            .Where(x => x.Contains(Separator.Text))
                            .Select(x => x.Split(Separator.Text))
                            .Where(x => x.Length == 2)
                            .Select(x => new QuizEntity { Question = x[0].Trim(), MainAnswer = x[1].Trim() });
                    }
                }

                if (Quizzes == null || !(AppendNew.IsChecked ?? true))
                    Quizzes = new List<QuizEntity>();
                if (entities != null)
                    Quizzes.AddRange(entities);

            }
            else
            {
                Quizzes = GetNew();
            }

            QuizzesGrid.ItemsSource = Quizzes;
            QuizzesGrid.Items.Refresh();
        }

        /// <summary>
        /// Сохраняет данные в файл
        /// </summary>
        private void SaveQuizzes()
        {
            if (!Directory.Exists(DataDirectory))
                Directory.CreateDirectory(DataDirectory);

            var workDir = Path.Combine(DataDirectory, "work");
            var imagesDir = Path.Combine(workDir, "images");
            string[] imageFiles;
            if (Directory.Exists(imagesDir))
            {
                imageFiles = Directory.GetFiles(imagesDir).ToArray();
            }
            else
            {
                Directory.CreateDirectory(imagesDir);
                imageFiles = Array.Empty<string>();
            }

            var ul = 0;
            foreach (var quiz in Quizzes)
            {
                // заново индексирует вопросы
                quiz.Id = ul++;
                if ((AddDefaultAuthor.IsChecked ?? false) && string.IsNullOrWhiteSpace(quiz.Author))
                    quiz.Author = DefaultAuthor.Text;

                // если для вопроса с указанной картинкой не существует файл, затирает guid
                if (quiz.ImageGuid != null)
                {
                    if (imageFiles.All(x => !x.Contains(quiz.ImageGuid.ToString())))
                        quiz.ImageGuid = null;
                }

                quiz.UseTimes = 0;
            }

            // все картинки, для которых нет guid в вопросах, удаляет для снижения размера результирующего файла
            foreach (var imageFile in imageFiles)
            {
                if (Quizzes.Where(x => x.HasImage).All(x => !imageFile.Contains(x.ImageGuid.ToString())))
                    File.Delete(imageFile);
            }

            using var stream = File.CreateText(Path.Combine(workDir, "quizzes.xml"));
            _serializer.Serialize(stream, Quizzes.ToArray());
            stream.Dispose();

            var outputPath = Path.Combine(DataDirectory, ToFile.Text);
            if (File.Exists(outputPath))
                File.Delete(outputPath);

            ZipFile.CreateFromDirectory(workDir, outputPath);
            ColorSaveButton(false);

            if (!QuizzesFileSizeWarning.IsChecked ?? true)
                return;

            var maxSize = 1_048_576L * (long.TryParse(QuizzesFileSizeWarningLimit.Text, out var i) ? i : -1); // 1048576 - байт в мегабайте
            if (maxSize < 0)
                return;

            var outFile = new FileInfo(outputPath);
            if (outFile.Length < maxSize)
                return;

            MessageBox.Show("Обратите внимание: размер файла сохранения достиг " + QuizzesFileSizeWarningLimit.Text +
                " МБ. Возможно, файл большего размера будет невозможно загрузить в Discord и, " +
                "следовательно, использовать для добавления вопросов. Рекомендуем начать новый" +
                " файл с вопросами.", "Вежливое предостережение.");
        }

        /// <summary>
        /// Открывает папку, хранящую документы с вопросами
        /// </summary>
        private void OpenFileDirectory()
            => Process.Start("explorer.exe", DataDirectory);

        /// <summary>
        /// Нажата кнопка сохранения
        /// </summary>
        private void ButtonSave_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveQuizzes();
                if (!(SaveAndLoadMode.IsChecked ?? false))
                    return;
                BuktopuhaFormat.IsChecked = true;
                FromFile.Text = ToFile.Text;
                LoadQuizzes();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        /// <summary>
        /// Нажата кнопка загрузки
        /// </summary>
        private void ButtonLoad_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                LoadQuizzes();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        /// <summary>
        /// Нажата кнопка "папка"
        /// </summary>
        private void OpenFileDirectoryButton_OnClick(object sender, RoutedEventArgs e) => OpenFileDirectory();

        /// <summary>
        /// Нажата кнопка "Сохранить настройки"
        /// </summary>
        private void SaveSettingsButton_OnClick(object sender, RoutedEventArgs e)
        {
            SaveSettings();
            MessageBox.Show("Настройки сохранены");
        }

        /// <summary>
        /// Нажата кнопка "Сброс настроек"
        /// </summary>
        private void DropSettingsButton_OnClick(object sender, RoutedEventArgs e) => LoadSettings();

        /// <summary>
        /// Перед закрытием окна - сохранить настройки
        /// </summary>
        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            var curSettings = new SettingsSaveObject
            {
                FromFile = FromFile.Text,
                ToFile = ToFile.Text,
                Separator = Separator.Text,
                BuktopuhaFormat = BuktopuhaFormat?.IsChecked ?? true,
                SaveAndLoadMode = SaveAndLoadMode?.IsChecked ?? true,
                DefaultAuthor = DefaultAuthor.Text,
                AddDefaultAuthor = AddDefaultAuthor?.IsChecked ?? true,
                ResizeImages = Resize.IsChecked ?? true,
                ResizeImageTo = int.Parse(ResizeTo.Text),
                QuizzesFileSizeLimit = int.Parse(QuizzesFileSizeWarningLimit.Text),
                QuizzesFileSizeWarning = QuizzesFileSizeWarning.IsChecked ?? false
            };
            if (!curSettings.Equals(_settings))
            {
                var result = MessageBox.Show(this, "Не все изменённые настройки были сохранены. Сохранить их перед выходом?", "Сохранить настройки?", MessageBoxButton.YesNoCancel);
                switch (result)
                {
                    case MessageBoxResult.Cancel:
                        e.Cancel = true;
                        break;
                    case MessageBoxResult.OK:
                    case MessageBoxResult.Yes:
                        SaveSettings();
                        break;
                    case MessageBoxResult.No:
                    case MessageBoxResult.None:
                        break;
                    default:
                        throw new Exception("Неизвестный тип ответа от диалогового окна.");
                }
            }
        }

        /// <summary>
        /// Управляет картинками вопроса в отдельном окне
        /// </summary>
        private void EditImage_OnClick(object sender, RoutedEventArgs e)
        {
            SelectedEntity = (QuizEntity)QuizzesGrid.SelectedValue;
            var imageWindow = new EditImageWindow(this)
            {
                Owner = this
            };
            imageWindow.ShowDialog();
            try
            {
                QuizzesGrid.Items.Refresh();
            }
            catch
            {
                MessageBox.Show("Не удалось обновить данные таблицы, так как идёт операция добавления нового элемента. " +
                                "Снимите курсор с добавляемого элемента и обновите таблицу через контекстное меню.", "Обновите данные таблицы");
            }
        }

        /// <summary>
        /// Обновляет данные в таблице
        /// </summary>
        private void Refresh_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                QuizzesGrid.Items.Refresh();
            }
            catch
            {
                MessageBox.Show("Не удалось обновить таблицу, так как идёт операция правки. " +
                                "Снимите курсор с изменяемой строки и повторите попытку.");
            }
        }

        /// <summary>
        /// Следит, чтобы в размере изображения было что-то вменяемое
        /// </summary>
        private void ResizeTo_OnLostFocus(object sender, RoutedEventArgs e)
        {
            var value = int.TryParse(ResizeTo.Text, out var i) ? i : -1;
            if (value > 1)
                return;

            MessageBox.Show("Значение в поле с размером изображения должно быть целым положительным числом. " +
                            "Значение возвращено к предыдущему приемлемому.");
            ResizeTo.Text = _settings.ResizeImageTo.ToString();
        }

        /// <summary>
        /// Следит, чтобы в размере ограничения файла было что-то вменяемое
        /// </summary>
        private void QuizzesFileSizeWarningLimit_OnLostFocus(object sender, RoutedEventArgs e)
        {
            var value = int.TryParse(QuizzesFileSizeWarningLimit.Text, out var i) ? i : -1;
            if (value > 0)
                return;

            MessageBox.Show("Значение в поле с ограничением по размеру файла сохранения должно быть целым положительным числом. " +
                            "Значение возвращено к предыдущему приемлемому.");
            QuizzesFileSizeWarningLimit.Text = _settings.QuizzesFileSizeLimit.ToString();
        }

        /// <summary>
        /// При изменении строк выделяет кнопку Сохранить цветом
        /// </summary>
        private void QuizzesGrid_OnRowEditEnding(object sender, DataGridRowEditEndingEventArgs e) 
            => ColorSaveButton();

        /// <summary>
        /// При изменении ячеек выделяет кнопку "сохранить" цветом
        /// </summary>
        private void QuizzesGrid_OnCellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
            => ColorSaveButton();

        /// <summary>
        /// Изменяет цвет кнопки "сохранить"
        /// </summary>
        /// <param name="color">если "да", будет зелёный оттенок</param>
        private void ColorSaveButton(bool color = true) => SaveButton.Background = color ? System.Windows.Media.Brushes.Aquamarine : default;
    }
}
