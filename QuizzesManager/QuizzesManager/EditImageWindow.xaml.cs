﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using Kaliko.ImageLibrary;
using Microsoft.Win32;

namespace QuizzesManager
{
    /// <summary>
    /// Interaction logic for EditImageWindow.xaml
    /// </summary>
    // ReSharper disable once RedundantExtendsListEntry
    public partial class EditImageWindow : Window
    {
        /// <summary>
        /// родительское окно
        /// </summary>
        private readonly MainWindow _parent;

        /// <summary>
        /// Загружает окно
        /// </summary>
        /// <param name="parent">Вызывающее окно</param>
        public EditImageWindow(MainWindow parent)
        {
            _parent = parent;
            InitializeComponent();

            var quiz = _parent.SelectedEntity;

            if (quiz == null)
            {
                MessageBox.Show("Не указан вопрос для управления картинкой.");
                Close();
                return;
            }

            Question.Text = "№ " + quiz.Id + ", от " + quiz.Author + "\r\n" + quiz.Question;

            if (quiz.ImageGuid != null)
            {
                var imagePath = Directory.GetFiles(Path.Combine(AppContext.BaseDirectory, "Data", "work", "images"))
                    .FirstOrDefault(x => x.Contains(quiz.ImageGuid.ToString()));
                if (string.IsNullOrWhiteSpace(imagePath))
                {
                    quiz.ImageGuid = null;
                }
                else
                {
                    var image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.UriSource = new Uri(imagePath);
                    image.EndInit();
                    Image.Source = image;
                }
            }
        }

        /// <summary>
        /// Добавляет изображение
        /// </summary>
        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            var picker = new OpenFileDialog();
            string imagePath = null;
            if (picker.ShowDialog() == true)
                imagePath = picker.FileName;

            if (string.IsNullOrWhiteSpace(imagePath) || !File.Exists(imagePath))
            {
                MessageBox.Show("Файл изображения не был выбран.");
                return;
            }

            var guid = Guid.NewGuid();
            var dir = Path.Combine(AppContext.BaseDirectory, "Data", "work", "images");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            var img = new KalikoImage(imagePath);

            if (_parent.Resize.IsChecked ?? true)
            {
                var width = (decimal)img.Size.Width;
                var height = (decimal)img.Size.Height;
                var settingsSize = int.TryParse(_parent.ResizeTo.Text, out var i) ? i : 300;
                var factor = 1m;

                if (width > settingsSize || height > settingsSize)
                    factor = width > height
                        ? width / settingsSize
                        : height / settingsSize;

                img.Resize((int)(width / factor), (int)(height / factor));
            }
            img.SavePng(Path.Combine(dir, guid + ".png"));

            _parent.SelectedEntity.ImageGuid = guid;
            var image = new BitmapImage();
            image.BeginInit();
            image.CacheOption = BitmapCacheOption.OnLoad;
            image.UriSource = new Uri(imagePath);
            image.EndInit();
            Image.Source = image;
        }

        /// <summary>
        /// Удаляет изображение
        /// </summary>
        private void DeleteButton_OnClick(object sender, RoutedEventArgs e)
        {
            var dir = Path.Combine(AppContext.BaseDirectory, "Data", "work", "images");
            var file = Directory.GetFiles(dir)
                .FirstOrDefault(x => x.Contains(_parent.SelectedEntity.ImageGuid.ToString()));
            _parent.SelectedEntity.ImageGuid = null;

            if (string.IsNullOrWhiteSpace(file) || !File.Exists(file))
                return;
            Image.Source = null;


            File.Delete(file);
        }

        /// <summary>
        /// Закрывает окно диалога
        /// </summary>
        private void EndButton_OnClick(object sender, RoutedEventArgs e) => Close();

    }
}
